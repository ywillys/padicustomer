<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Adm_login extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->model('user_m','user_m');
    }
    public function index(){
        // $data['password'] = "indri83012020";
        // $data['password'] = "dwi83022020";
        // $data['password'] = "ketut83042020";
        // $pengacak = "1S4I0V2I1A9A9Z7IZAH";
        // $data['passwordnew'] = password_hash($data['password'].$pengacak, PASSWORD_DEFAULT);
        // echo $data['passwordnew'];
    
        $this->load->view('admin/login_v');
    }
    public function submit_login(){
        $data['username'] = $this->db->escape_str( $this->input->post('username') );
        $data['password'] = $this->db->escape_str( $this->input->post('password') );

        if ($data['username'] != "" && $data['password'] != ""){
            $this->authentification($data['username'], $data['password']);
        }
        else {
            $this->session->set_flashdata('select', 'Pastikan anda mengisikan input.');
            redirect(base_url());
        }

    }
    private function authentification($username, $password){
        $data['username'] = $username;
        $data['password'] = $password;
        $pengacak = "1S4I0V2I1A9A9Z7IZAH";
//        $data['passwordnew'] = password_hash($data['password'].$pengacak, PASSWORD_DEFAULT);
//        echo $data['passwordnew'];
        $hasil = $this->user_m->get_user($username);
        var_dump($hasil);
        if ($hasil['jumlah'] > 0){
            $dataresult = $hasil['result'][0];
            if(password_verify($data['password'].$pengacak, $dataresult['password'])){
                $dataresult = $hasil['result'][0];

                $data['logged_in'] = "loggedin";

                $data['id_user'] = $dataresult['id_user'];
                $data['username'] = $dataresult['username'];
                $data['nama_lengkap'] = $dataresult['nama_lengkap'];
                $data['level'] = $dataresult['level'];
                $data['status_aktif'] = $dataresult['status_aktif'];
    
                $data_hist['id_user'] = $data['id_user'];
                $data_hist['time'] = $this->today_datetime();
                $this->user_m->insert_user_history($data_hist);

                $data_fail['username'] = $username;
                $data_fail['password'] = $password;
                $this->user_m->insert_user_failed_login($data_fail);
    
                $this->session->set_userdata($data);
                $this->session->set_flashdata('sukses', 'Selamat datang !');
                redirect(base_url().'admin/dashboard');
            }
            else {
                $this->session->set_flashdata('stop', 'Username atau Password Salah !');
                redirect(base_url());
            }
        }
        else {
            $data_fail['username'] = $username;
            $data_fail['password'] = $password;
            $data_fail['time'] = $this->today_datetime();
            $this->user_m->insert_user_failed_login($data_fail);

            $this->session->set_flashdata('select', 'Mohon periksa kembali email dan password anda.');
            redirect(base_url());
        }
    }
    public function logged_in(){
        if($this->session->userdata('logged_in')){
            $usr = $this->session->userdata('username');
            //echo $usr
            return isset($usr);
        }
    }
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url().'adm_login');
    }

    private function today_datetime(){
        $datestring = '%Y-%m-%d %h:%i:%s';
        $time = time();
        $tanggal = mdate($datestring, $time);

        return $tanggal;
    }

}
?>