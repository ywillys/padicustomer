<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Filterpelanggan extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->model('fungsional_pic_m','fungsional_pic_m');
        $this->load->model('layanan_pelanggan_m','layanan_pelanggan_m');
        $this->load->model('jenis_layanan_m','jenis_layanan_m');
        $this->load->model('jabatan_pic_m','jabatan_pic_m');
        $this->load->model('kabupaten_m','kabupaten_m');
        $this->load->model('kategori_layanan_m','kategori_layanan_m');
        $this->load->model('kategori_berlangganan_m','kategori_berlangganan_m');
        $this->load->model('kecamatan_m','kecamatan_m');
        $this->load->model('kelurahan_m','kelurahan_m');
        $this->load->model('layanan_m','layanan_m');
        $this->load->model('layanan_pelanggan_m','layanan_pelanggan_m');
        $this->load->model('provinsi_m','provinsi_m');
        $this->load->model('profil_pelanggan_m','profil_pelanggan_m');
        $this->load->model('sektor_industri_m','sektor_industri_m');
        $this->load->model('user_m','user_m');
        $this->load->model('yurisdiksi_m','yurisdiksi_m');

        if($this->logged_in_cus()){
		}
		else {
			$this->session->set_flashdata('select', 'Periksa userlogin anda !');
			redirect(base_url());
		}
    }
    public function index(){
        $api_pelanggan = $this->api_pelanggan();
        $list_kategori_berlangganan = $this->kategori_berlangganan_m->select_all();
        $list_provinsi = $this->provinsi_m->select_all();
        $list_sektor_industri = $this->sektor_industri_m->select_all();
        $list_jenis_layanan = $this->jenis_layanan_m->select_all();
        $list_user = $this->user_m->select_all();
        $data = array(
            'css_file' => array(
                'assets/main.css',
                'assets/add.css'
            ),
            'css_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css'
            ),
            'js_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js'
            ),
            'js_file' => array(
                'assets/scripts/main.js',
                'assets/scripts/validate_form.js',
                'assets/scripts/filter_input.js'
            ),
            'template' => 'filterinput',
            'menu' => 'filter',
            'api_pelanggan' => $api_pelanggan,
            'list_jenis_layanan' => $list_jenis_layanan,
            'list_kategori_berlangganan' => $list_kategori_berlangganan,
            'list_provinsi' => $list_provinsi,
            'list_sektor_industri' => $list_sektor_industri,
            'list_user' => $list_user
        );

        
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', ' ');
        $this->form_validation->set_rules('input_status', 'input_status', 'required');
        if ($this->form_validation->run()==FALSE){
            $this->load->view('admin/templateroot_v', $data);
        }
        else {
            $data_fil['level_perusahaan'] = $this->input->post('level_perusahaan');
            $data_fil['id_sektor_industri'] = $this->input->post('id_sektor_industri');
            $data_fil['id_kategori_berlangganan'] = $this->input->post('id_kategori_berlangganan');
            $data_fil['id_provinsi'] = $this->input->post('id_provinsi');
            $data_fil['id_kecamatan'] = $this->input->post('id_kecamatan');
            $data_fil['id_kabupaten'] = $this->input->post('id_kabupaten');
            $data_fil['id_kelurahan'] = $this->input->post('id_kelurahan');
            $data_fil['id_jenis_layanan'] = $this->input->post('id_jenis_layanan');
            $data_fil['id_kategori_layanan'] = $this->input->post('id_kategori_layanan');
            $data_fil['id_layanan'] = $this->input->post('id_layanan');
            $data_fil['tgl_start'] = $this->input->post('tgl_start');
            $data_fil['tgl_end'] = $this->input->post('tgl_end');
            $data_fil['nominal_start'] = $this->input->post('nominal_start');
            $data_fil['nominal_end'] = $this->input->post('nominal_end');
            $data_fil['status_filter'] = "getdatafilter";


            $this->session->set_userdata($data_fil);
            $this->session->set_flashdata('sukses', 'Menampilkan hasil filter');
            redirect(base_url().'admin/filterpelanggan/filter_show/');
        }
    }

    public function filter_show() {
        if ($this->session->userdata('status_filter')){
            $level_perusahaan = $this->session->userdata('level_perusahaan');
            $id_sektor_industri = $this->session->userdata('id_sektor_industri');
            $id_kategori_berlangganan = $this->session->userdata('id_kategori_berlangganan');
            $id_provinsi = $this->session->userdata('id_provinsi');
            $id_kecamatan = $this->session->userdata('id_kecamatan');
            $id_kabupaten = $this->session->userdata('id_kabupaten');
            $id_kelurahan = $this->session->userdata('id_kelurahan');
            $id_jenis_layanan = $this->session->userdata('id_jenis_layanan');
            $id_kategori_layanan = $this->session->userdata('id_kategori_layanan');
            $id_layanan = $this->session->userdata('id_layanan');
            $tgl_start = $this->session->userdata('tgl_start');
            $tgl_end = $this->session->userdata('tgl_end');
            $nominal_start = $this->session->userdata('nominal_start');
            $nominal_end = $this->session->userdata('nominal_end');

            $data_sektor_industri = $this->sektor_industri_m->select_detil_sektor_industri($id_sektor_industri);
            $data_kategori_berlangganan = $this->kategori_berlangganan_m->select_detil_kategori_berlangganan($id_kategori_berlangganan);
            $data_provinsi = $this->provinsi_m->select_detil_provinsi($id_provinsi);
            $data_kecamatan = $this->kecamatan_m->select_detil_kecamatan($id_kecamatan);
            $data_kabupaten = $this->kabupaten_m->select_detil_kabupaten($id_kabupaten);
            $data_kelurahan = $this->kelurahan_m->select_detil_kelurahan($id_kelurahan);
            $data_jenis_layanan = $this->jenis_layanan_m->select_detil_jenis_layanan($id_jenis_layanan);
            $data_kategori_layanan = $this->kategori_layanan_m->select_detil_kategori_layanan($id_kategori_layanan);
            $data_layanan = $this->layanan_m->select_detil_layanan($id_layanan);
            
            $filter_arr_nom = array();
            $filter_arr = array();
            
            if(!empty($level_perusahaan)){
                $filter_arr['pp.level_perusahaan'] = $level_perusahaan;
            }
            if(!empty($id_sektor_industri)){
                $filter_arr['pp.id_sektor_industri'] = $id_sektor_industri;
            }
            if(!empty($id_kategori_berlangganan)){
                $filter_arr['pp.id_kategori_berlangganan'] = $id_kategori_berlangganan;
            }
            if(!empty($id_provinsi)){
                $filter_arr['pp.id_provinsi'] = $id_provinsi;
            }
            if(!empty($id_kecamatan)){
                $filter_arr['pp.id_kecamatan'] = $id_kecamatan;
            }
            if(!empty($id_kabupaten)){
                $filter_arr['pp.id_kabupaten'] = $id_kabupaten;
            }
            if(!empty($id_kelurahan)){
                $filter_arr['pp.id_kelurahan'] = $id_kelurahan;
            }
            if(!empty($id_jenis_layanan)){
                $filter_arr['lp.id_jenis_layanan'] = $id_jenis_layanan;
            } 
            if(!empty($id_kategori_layanan)){
                $filter_arr['lp.id_kategori_layanan'] = $id_kategori_layanan;
            } 
            if(!empty($id_layanan)){
                $filter_arr['lp.id_layanan'] = $id_layanan;
            }
            if(!empty($tgl_start)){
                $filter_arr['lp.tgl_mulai >='] = $tgl_start;
            }
            if(!empty($tgl_end)){
                $filter_arr['lp.tgl_mulai <='] = $tgl_end;
            } 
            if(!empty($nominal_start)){
                $filter_arr_nom['SUM(lp.nominal) >='] = $nominal_start;
            }
            if(!empty($nominal_end)){
                $filter_arr_nom['SUM(lp.nominal) <='] = $nominal_end;
            }   
            if (empty($filter_arr) && empty($filter_arr_nom)) {
                $this->session->set_flashdata('stop', 'Isikan parameter filter dahulu.');
                redirect(base_url().'admin/filterpelanggan/');
            }
            if (!empty($tgl_start) && empty($tgl_end)) {
                $this->session->set_flashdata('stop', 'Range tanggal harus diisi keduanya !');
                redirect(base_url().'admin/filterpelanggan/');
            }
            if (empty($tgl_start) && !empty($tgl_end)) {
                $this->session->set_flashdata('stop', 'Range tanggal harus diisi keduanya !');
                redirect(base_url().'admin/filterpelanggan/');
            }
            if (!empty($nominal_start) && empty($nominal_end)) {
                $this->session->set_flashdata('stop', 'Range nominal harus diisi keduanya !');
                redirect(base_url().'admin/filterpelanggan/');
            }
            if (empty($nominal_start) && !empty($nominal_end)) {
                $this->session->set_flashdata('stop', 'Range nominal harus diisi keduanya !');
                redirect(base_url().'admin/filterpelanggan/');
            }

            if(!empty($tgl_start)){
                $tgl_start_new = $this->nama_bulan($tgl_start);
            } else {
                $tgl_start_new = $tgl_start; //agar tanggal kosong tidak diformat dengan nama bulan
            }

            if(!empty($tgl_end)){
                $tgl_end_new = $this->nama_bulan($tgl_end);
            } else {
                $tgl_end_new = $tgl_end; //agar tanggal kosong tidak diformat dengan nama bulan
            }

            $list_profil_pelanggan = $this->profil_pelanggan_m->filter_harga_layanan($filter_arr, $filter_arr_nom);

            $data = array(
                'css_file' => array(
                    'assets/main.css',
                    'assets/add.css'
                ),
                'css_link_plugin' => array(
                    ''
                ),
                'js_link_plugin' => array(
                    'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'
                ),
                'js_file' => array(
                    'assets/scripts/main.js'
                ),
                'template' => 'filtershow',
                'menu' => 'filter',
                'list_profil_pelanggan' => $list_profil_pelanggan,
                'level_perusahaan' => $level_perusahaan,
                'data_sektor_industri' => $data_sektor_industri,
                'data_kategori_berlangganan' => $data_kategori_berlangganan,
                'data_provinsi' => $data_provinsi,
                'data_kecamatan' => $data_kecamatan,
                'data_kabupaten' => $data_kabupaten,
                'data_kelurahan' => $data_kelurahan,
                'data_jenis_layanan' => $data_jenis_layanan,
                'data_kategori_layanan' => $data_kategori_layanan,
                'data_layanan' => $data_layanan,
                'tgl_start' => $tgl_start_new,
                'tgl_end' => $tgl_end_new,
                'nominal_start' => $nominal_start,
                'nominal_end' => $nominal_end,
            );
            
            $this->load->view('admin/templateroot_v', $data);
        }
        else {
            $this->session->set_flashdata('stop', 'Isikan parameter filter lebih dulu.');
            redirect(base_url().'admin/filterpelanggan/');
        }
    }

    public function get_alamat_pelanggan($id_pelanggan){
        if($id_pelanggan != ''){
            $datapelanggan = $this->api_pelanggan();
            foreach ($datapelanggan as $row){
                if ($row['id'] == $id_pelanggan){
                    $alamat_pelanggan = $row['address'];
                }
            }
            echo $alamat_pelanggan;
        }
    }

    public function get_kabupaten($id_provinsi, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_provinsi != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            
            $data['list_kabupaten'] = $this->kabupaten_m->select_all_provinsi($id_provinsi);
            $this->load->view('admin/kabupaten_show_v', $data);
        }
    }

    public function get_kecamatan($id_kabupaten, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_kabupaten != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            $data['list_kecamatan'] = $this->kecamatan_m->select_all_kabupaten($id_kabupaten);
            $this->load->view('admin/kecamatan_show_v', $data);
         }
    }

    public function get_kelurahan($id_kecamatan, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_kecamatan != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            $data['list_kelurahan'] = $this->kelurahan_m->select_all_kecamatan($id_kecamatan);
            $this->load->view('admin/kelurahan_show_v', $data);
         }
    }

    public function get_kategori_layanan($id_jenis_layanan){
        if($id_jenis_layanan != ''){
            $data['list_kategori_layanan'] = $this->kategori_layanan_m->select_all_jenis($id_jenis_layanan);
            
            $this->load->view('admin/kategori_layanan_show_v', $data);
         }
    }

    public function get_layanan($id_kategori_layanan){
        if($id_kategori_layanan != ''){
            $data['list_layanan'] = $this->layanan_m->select_all_kategori($id_kategori_layanan);
            $this->load->view('admin/layanan_show_v', $data);
         }
    }

    private function api_pelanggan_sites() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.117:2020/getallclientsites');
        // http://192.168.0.117:2020/getclients
        // http://192.168.0.117:2020/getclientsbyname
        // http://192.168.0.117:2020/getallclientsites
        // http://192.168.0.117:2020/getclientsitesbyclientid/19 
        // 19 contoh client_id
        // dari mas puji
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        
        foreach ($obj as $obj_pelanggan){
            $api_pelanggan_sites[] = json_decode(json_encode($obj_pelanggan),true);
        }

        // var_dump($api_pelanggan_sites);

        return $api_pelanggan_sites;
    }

    public function api_pelanggan() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.117:2020/getclients');
        // http://192.168.0.117:2020/getclients
        // http://192.168.0.117:2020/getclientsbyname
        // http://192.168.0.117:2020/getallclientsites
        // http://192.168.0.117:2020/getclientsitesbyclientid/19 
        // 19 contoh client_id
        // dari mas puji
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        
        foreach ($obj as $obj_pelanggan){
            $api_pelanggan[] = json_decode(json_encode($obj_pelanggan),true);
        }

        // var_dump($api_pelanggan);
        return $api_pelanggan;
    }

    private function nama_bulan($tgl){
        $tanggal_full = date_create($tgl);
        $date = date_format($tanggal_full,'d M Y');
        $format = array(
            'Jan' => 'Januari',
            'Feb' => 'Februari',
            'Mar' => 'Maret',
            'Apr' => 'April',
            'May' => 'Mei',
            'Jun' => 'Juni',
            'Jul' => 'Juli',
            'Aug' => 'Agustus',
            'Sep' => 'September',
            'Oct' => 'Oktober',
            'Nov' => 'November',
            'Dec' => 'Desember'
        );
        $new_tgl = strtr(($date),$format);
        return $new_tgl;
    }
    
    private function today_datetime(){
        $datestring = '%Y-%m-%d %h:%i:%s';
        $time = time();
        $tanggal = mdate($datestring, $time);

        return $tanggal;
    }

    private function logged_in_cus(){
        if($this->session->userdata('logged_in')){
            $usr = $this->session->userdata('username');
            return isset($usr);
        }
    }
}
?>