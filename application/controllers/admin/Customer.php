<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Customer extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->model('fungsional_pic_m','fungsional_pic_m');
        $this->load->model('layanan_pelanggan_m','layanan_pelanggan_m');
        $this->load->model('jenis_layanan_m','jenis_layanan_m');
        $this->load->model('jabatan_pic_m','jabatan_pic_m');
        $this->load->model('kabupaten_m','kabupaten_m');
        $this->load->model('kategori_layanan_m','kategori_layanan_m');
        $this->load->model('kategori_berlangganan_m','kategori_berlangganan_m');
        $this->load->model('kecamatan_m','kecamatan_m');
        $this->load->model('kelurahan_m','kelurahan_m');
        $this->load->model('layanan_m','layanan_m');
        $this->load->model('layanan_pelanggan_m','layanan_pelanggan_m');
        $this->load->model('pic_pelanggan_m','pic_pelanggan_m');
        $this->load->model('provinsi_m','provinsi_m');
        $this->load->model('profil_pelanggan_m','profil_pelanggan_m');
        $this->load->model('sektor_industri_m','sektor_industri_m');
        $this->load->model('user_m','user_m');
        $this->load->model('yurisdiksi_m','yurisdiksi_m');

        if($this->logged_in_cus()){
		}
		else {
			$this->session->set_flashdata('select', 'Periksa userlogin anda !');
			redirect(base_url());
		}
    }
    public function index(){
        if (!empty($this->session->userdata('tab_menu_layanan'))){
            $this->session->unset_userdata('tab_menu_layanan');
        }

        $list_profil_pelanggan = $this->profil_pelanggan_m->select_all();
        $i=0;
        foreach($list_profil_pelanggan as $row){
            $list_det_layanan_pelanggan = $this->layanan_pelanggan_m->select_all_pelanggan($row['id_profil_pelanggan']);
            if(!empty($list_det_layanan_pelanggan)){
                $list_profil_pelanggan[$i]['status_layanan'] = 1;
            } else {
                $list_profil_pelanggan[$i]['status_layanan'] = 0;
            }
            $i++;
        }
        // var_dump($list_profil_pelanggan);
        $data = array(
            'css_file' => array(
                'assets/main.css'
            ),
            'css_link_plugin' => array(
                ''
            ),
            'js_link_plugin' => array(
               ''
            ),
            'js_file' => array(
                'assets/scripts/main.js'
            ),
            'template' => 'customer',
            'menu' => 'customer',
            'list_profil_pelanggan' => $list_profil_pelanggan
        );

        $this->load->view('admin/templateroot_v', $data);
    }

    public function tambah_data() {
        if (!empty($this->session->userdata('tab_menu_layanan'))){
            $this->session->unset_userdata('tab_menu_layanan');
        }

        $api_pelanggan = $this->api_pelanggan();
        $list_fungsional_pic = $this->fungsional_pic_m->select_all();
        $list_layanan_pelanggan = $this->layanan_pelanggan_m->select_all();
        $i=0;
        foreach($list_layanan_pelanggan as $row){
            $list_layanan_pelanggan[$i]['tgl_mulai_new'] = $this->nama_bulan($list_layanan_pelanggan[$i]['tgl_mulai']);
            $i++;
        }
        $list_jabatan_pic = $this->jabatan_pic_m->select_all();
        $list_kategori_berlangganan = $this->kategori_berlangganan_m->select_all();
        $list_provinsi = $this->provinsi_m->select_all();
        $list_profil_pelanggan = $this->profil_pelanggan_m->select_all();
        $list_sektor_industri = $this->sektor_industri_m->select_all();
        $list_user = $this->user_m->select_all();
        $list_yurisdiksi = $this->yurisdiksi_m->select_all();
        $data = array(
            'css_file' => array(
                'assets/main.css',
                'assets/add.css'
            ),
            'css_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css'
            ),
            'js_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js'
            ),
            'js_file' => array(
                'assets/scripts/main.js',
                'assets/scripts/validate_form.js',
                'assets/scripts/pelanggan_input.js'
            ),
            'template' => 'customertambahdata',
            'menu' => 'customer',
            'api_pelanggan' => $api_pelanggan,
            'list_fungsional_pic' => $list_fungsional_pic,
            'list_layanan_pelanggan' => $list_layanan_pelanggan,
            'list_jabatan_pic' => $list_jabatan_pic,
            'list_kategori_berlangganan' => $list_kategori_berlangganan,
            'list_provinsi' => $list_provinsi,
            'list_profil_pelanggan' => $list_profil_pelanggan,
            'list_sektor_industri' => $list_sektor_industri,
            'list_user' => $list_user,
            'list_yurisdiksi' => $list_yurisdiksi
        );

        
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', ' ');
        $this->form_validation->set_rules('nama_pelanggan_update', 'nama_pelanggan_update', 'required');
        if ($this->form_validation->run()==FALSE){
            $this->load->view('admin/templateroot_v', $data);
        }
        else {
            $jum_pel = $this->profil_pelanggan_m->jum_profil_pelanggan();
            $next_id = $jum_pel + 1;

            $data_formco['id_profil_pelanggan'] = $next_id;
            $data_formco['nama_pelanggan_update'] = $this->input->post('nama_pelanggan_update');
            $data_formco['id_pelanggan'] = $this->input->post('id_pelanggan');
            $data_formco['alamat_pelanggan_update'] = $this->input->post('alamat_pelanggan_update');
            $data_formco['level_perusahaan'] = $this->input->post('level_perusahaan');
            $data_formco['id_sektor_industri'] = $this->input->post('id_sektor_industri');
            $data_formco['id_kategori_berlangganan'] = $this->input->post('id_kategori_berlangganan');
            $data_formco['id_provinsi'] = $this->input->post('id_provinsi');
            $data_formco['id_kecamatan'] = $this->input->post('id_kecamatan');
            $data_formco['id_kabupaten'] = $this->input->post('id_kabupaten');
            $data_formco['id_kelurahan'] = $this->input->post('id_kelurahan');
            $data_formco['email'] = $this->input->post('email');
            $data_formco['instagram'] = $this->input->post('instagram');
            $data_formco['facebook'] = $this->input->post('facebook');
            $data_formco['linkedin'] = $this->input->post('linkedin');
            // $data_formco['nama_dm'] = $this->input->post('nama_dm');
            // $data_formco['id_fungsional_dm'] = $this->input->post('id_fungsional_dm');
            // $data_formco['id_jabatan_dm'] = $this->input->post('id_jabatan_dm');
            // $data_formco['no_telp_dm'] = $this->input->post('no_telp_dm');
            // $data_formco['nama_influ'] = $this->input->post('nama_influ');
            // $data_formco['id_fungsional_influ'] = $this->input->post('id_fungsional_influ');
            // $data_formco['id_jabatan_influ'] = $this->input->post('id_jabatan_influ');
            // $data_formco['no_telp_influ'] = $this->input->post('no_telp_influ');
            $data_formco['id_user'] = $this->session->userdata('id_user');
            $data_formco['time'] = $this->today_datetime();

            if(!empty($this->input->post('nama_pemohon'))){
                $data_pemohon['id_profil_pelanggan'] = $next_id;
                $data_pemohon['nama_pic'] =  $this->input->post('nama_pemohon');
                $data_pemohon['no_telp'] =  $this->input->post('no_telp_pemohon');
                $data_pemohon['email'] =  $this->input->post('email_pemohon');
                $data_pemohon['hobi'] =  $this->input->post('hobi_pemohon');
                $data_pemohon['agama'] =  $this->input->post('agama_pemohon');
                $data_pemohon['role'] =  $this->input->post('role_pemohon');
                $data_pemohon['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_pemohon);
            }
            if(!empty($this->input->post('nama_penanggung'))){
                $data_penanggung['id_profil_pelanggan'] = $next_id;
                $data_penanggung['nama_pic'] =  $this->input->post('nama_penanggung');
                $data_penanggung['no_telp'] =  $this->input->post('no_telp_penanggung');
                $data_penanggung['email'] =  $this->input->post('email_penanggung');
                $data_penanggung['hobi'] =  $this->input->post('hobi_penanggung');
                $data_penanggung['agama'] =  $this->input->post('agama_penanggung');
                $data_penanggung['role'] =  $this->input->post('role_penanggung');
                $data_penanggung['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_penanggung);
            }
            if(!empty($this->input->post('nama_adm'))){
                $data_adm['id_profil_pelanggan'] = $next_id;
                $data_adm['nama_pic'] =  $this->input->post('nama_adm');
                $data_adm['no_telp'] =  $this->input->post('no_telp_adm');
                $data_adm['email'] =  $this->input->post('email_adm');
                $data_adm['hobi'] =  $this->input->post('hobi_adm');
                $data_adm['agama'] =  $this->input->post('agama_adm');
                $data_adm['role'] =  $this->input->post('role_adm');
                $data_adm['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_adm);
            }
            if(!empty($this->input->post('nama_teknis'))){
                $data_teknis['id_profil_pelanggan'] = $next_id;
                $data_teknis['nama_pic'] =  $this->input->post('nama_teknis');
                $data_teknis['no_telp'] =  $this->input->post('no_telp_teknis');
                $data_teknis['email'] =  $this->input->post('email_teknis');
                $data_teknis['hobi'] =  $this->input->post('hobi_teknis');
                $data_teknis['agama'] =  $this->input->post('agama_teknis');
                $data_teknis['role'] =  $this->input->post('role_teknis');
                $data_teknis['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_teknis);
            }
            if(!empty($this->input->post('nama_billing'))){
                $data_billing['id_profil_pelanggan'] = $next_id;
                $data_billing['nama_pic'] =  $this->input->post('nama_billing');
                $data_billing['no_telp'] =  $this->input->post('no_telp_billing');
                $data_billing['email'] =  $this->input->post('email_billing');
                $data_billing['hobi'] =  $this->input->post('hobi_billing');
                $data_billing['agama'] =  $this->input->post('agama_billing');
                $data_billing['role'] =  $this->input->post('role_billing');
                $data_billing['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_billing);
            }
            if(!empty($this->input->post('nama_support'))){
                $data_support['id_profil_pelanggan'] = $next_id;
                $data_support['nama_pic'] =  $this->input->post('nama_support');
                $data_support['no_telp'] =  $this->input->post('no_telp_support');
                $data_support['email'] =  $this->input->post('email_support');
                $data_support['hobi'] =  $this->input->post('hobi_support');
                $data_support['agama'] =  $this->input->post('agama_support');
                $data_support['role'] =  $this->input->post('role_support');
                $data_support['time'] = $this->today_datetime();

                $this->pic_pelanggan_m->insert_pic_pelanggan($data_support);
            }
            // echo $this->input->post('nama_pic_pemohon');
            // var_dump($data_pemohon);

            $this->profil_pelanggan_m->insert_profil_pelanggan($data_formco);
            $this->session->set_flashdata('sukses', 'Anda telah berhasil menambahkan Customer : <b>'.$data_formco['nama_pelanggan_update'].'</b>');
            redirect(base_url().'admin/customer/detil_data/'.$next_id);
        }
    }

    public function detil_data ($id_profil_pelanggan) {
        if (empty($this->session->userdata('tab_menu_layanan'))){
            $datamenulayanan['tab_menu_layanan'] = 'profile'; 
            $this->session->set_userdata($datamenulayanan);
        } 

        $api_pelanggan = $this->api_pelanggan();
        $list_layanan_pelanggan = $this->layanan_pelanggan_m->select_all();
        $i=0;
        foreach($list_layanan_pelanggan as $row){
            $list_layanan_pelanggan[$i]['tgl_mulai_new'] = $this->nama_bulan($list_layanan_pelanggan[$i]['tgl_mulai']);
            $i++;
        }
        $list_jenis_layanan = $this->jenis_layanan_m->select_all();
        $list_kategori_layanan = $this->kategori_layanan_m->select_all();
        $list_layanan = $this->layanan_m->select_all();
        $list_profil_pelanggan = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
        $list_det_layanan_pelanggan = $this->layanan_pelanggan_m->select_all_pelanggan($id_profil_pelanggan);
        $list_pic_pelanggan = $this->pic_pelanggan_m->select_all_pelanggan($id_profil_pelanggan);

        if(!empty($list_det_layanan_pelanggan)){
            $i=0;
            foreach ($list_det_layanan_pelanggan as $row){
                $list_det_layanan_pelanggan[$i]['tgl_mulai_new'] = $this->nama_bulan($list_det_layanan_pelanggan[$i]['tgl_mulai']);
                $i++;
            }
        }
        $data = array(
            'css_file' => array(
                'assets/main.css',
                'assets/add.css'
            ),
            'css_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css'
            ),
            'js_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js'
            ),
            'js_file' => array(
                'assets/scripts/main.js',
                'assets/scripts/validate_form.js',
                'assets/scripts/pelanggan_detil.js'
            ),
            'footer' => array(
                '/admin/customer_edit_modal_v'
            ),
            'template' => 'customerdetildata',
            'menu' => 'customer',
            'api_pelanggan' => $api_pelanggan,
            'list_layanan_pelanggan' => $list_layanan_pelanggan,
            'list_jenis_layanan' => $list_jenis_layanan,
            'list_kategori_layanan' => $list_kategori_layanan,
            'list_layanan' => $list_layanan,
            'list_profil_pelanggan' => $list_profil_pelanggan,
            'list_kategori_layanan' => $list_kategori_layanan,
            'list_det_layanan_pelanggan' => $list_det_layanan_pelanggan,
            'list_pic_pelanggan' => $list_pic_pelanggan
        );

        $this->load->library('form_validation');
        $this->form_validation->set_message('required', ' ');
        $this->form_validation->set_rules('id_layanan', 'id_layanan', 'required');
        if ($this->form_validation->run()==FALSE){
            $this->load->view('admin/templateroot_v', $data);
        }
        else {
            $data_formla['id_profil_pelanggan'] = $id_profil_pelanggan;
            $data_formla['id_jenis_layanan'] = $this->input->post('id_jenis_layanan');
            $data_formla['id_kategori_layanan'] = $this->input->post('id_kategori_layanan');
            $data_formla['id_layanan'] = $this->input->post('id_layanan');
            $data_formla['nominal'] = $this->input->post('nominal');
            $data_formla['tgl_mulai'] = $this->input->post('tgl_mulai');
            $data_formla['id_user'] = $this->session->userdata('id_user');
            $data_formla['time'] = $this->today_datetime();

            $datamenulayanan['tab_menu_layanan'] = 'layanan'; 
            $this->session->set_userdata($datamenulayanan);

            $this->layanan_pelanggan_m->insert_layanan_pelanggan($data_formla);
            $this->session->set_flashdata('sukses', 'Anda telah berhasil menambahkan layanan');
            redirect(base_url().'admin/customer/detil_data/'.$id_profil_pelanggan);
            
        }
    }


    public function edit_data ($id_profil_pelanggan) {
        if (empty($this->session->userdata('tab_menu_layanan'))){
            $datamenulayanan['tab_menu_layanan'] = 'profile'; 
            $this->session->set_userdata($datamenulayanan);
        } 

        $list_profil_pelanggan = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
        $list_det_layanan_pelanggan = $this->layanan_pelanggan_m->select_all_pelanggan($id_profil_pelanggan);

        if(!empty($list_det_layanan_pelanggan)){
            $i=0;
            foreach ($list_det_layanan_pelanggan as $row){
                $list_det_layanan_pelanggan = $this->nama_bulan($list_det_layanan_pelanggan[$i]['tgl_mulai']);
                $i++;
            }
        }
        $api_pelanggan = $this->api_pelanggan();
        $list_fungsional_pic = $this->fungsional_pic_m->select_all();
        $list_layanan_pelanggan = $this->layanan_pelanggan_m->select_all();
        $i=0;
        foreach($list_layanan_pelanggan as $row){
            $list_layanan_pelanggan[$i]['tgl_mulai_new'] = $this->nama_bulan($list_layanan_pelanggan[$i]['tgl_mulai']);
            $i++;
        }
        $list_jabatan_pic = $this->jabatan_pic_m->select_all(1);
        $list_jenis_layanan = $this->jenis_layanan_m->select_all();
        $list_kategori_layanan = $this->kategori_layanan_m->select_all();
        $list_kategori_berlangganan = $this->kategori_berlangganan_m->select_all();
		$list_layanan = $this->layanan_m->select_all();
        $list_provinsi = $this->provinsi_m->select_all();
        $list_sektor_industri = $this->sektor_industri_m->select_all();
        $list_user = $this->user_m->select_all();
        $list_yurisdiksi = $this->yurisdiksi_m->select_all();
        $data = array(
            'css_file' => array(
                'assets/main.css',
                'assets/add.css'
            ),
            'css_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css',
                'https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css'
            ),
            'js_link_plugin' => array(
                'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js'
            ),
            'js_file' => array(
                'assets/scripts/main.js',
                'assets/scripts/validate_form.js',
                'assets/scripts/pelanggan_edit.js'
            ),
            'template' => 'customereditdata',
                
            'api_pelanggan' => $api_pelanggan,
            'template' => 'customereditdata',
            'menu' => 'customer',
            'list_det_layanan_pelanggan' => $list_det_layanan_pelanggan,
            'list_fungsional_pic' => $list_fungsional_pic,
            'list_layanan_pelanggan' => $list_layanan_pelanggan,
            'list_jabatan_pic' => $list_jabatan_pic,
            'list_kategori_layanan' => $list_kategori_layanan,
            'list_kategori_berlangganan' => $list_kategori_berlangganan,
            'list_layanan' => $list_layanan,
            'list_provinsi' => $list_provinsi,
            'list_profil_pelanggan' => $list_profil_pelanggan,
            'list_sektor_industri' => $list_sektor_industri,
            'list_user' => $list_user,
            'list_yurisdiksi' => $list_yurisdiksi,
            'id_profil_pelanggan' => $id_profil_pelanggan
        );
        
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', ' ');
        $this->form_validation->set_rules('id_layanan', 'id_layanan', 'required');
        if ($this->form_validation->run()==FALSE){
            $this->load->view('admin/templateroot_v', $data);
        }
        else {
            $data_formco['nama_pelanggan_update'] = $this->input->post('nama_pelanggan_update');
            $data_formco['id_pelanggan'] = $this->input->post('id_pelanggan');
            $data_formco['alamat_pelanggan_update'] = $this->input->post('alamat_pelanggan_update');
            $data_formco['level_perusahaan'] = $this->input->post('level_perusahaan');
            $data_formco['id_sektor_industri'] = $this->input->post('id_sektor_industri');
            $data_formco['id_kategori_berlangganan'] = $this->input->post('id_kategori_berlangganan');
            $data_formco['id_provinsi'] = $this->input->post('id_provinsi');
            $data_formco['id_kecamatan'] = $this->input->post('id_kecamatan');
            $data_formco['id_kabupaten'] = $this->input->post('id_kabupaten');
            $data_formco['id_kelurahan'] = $this->input->post('id_kelurahan');
            $data_formco['email'] = $this->input->post('email');
            $data_formco['instagram'] = $this->input->post('instagram');
            $data_formco['facebook'] = $this->input->post('facebook');
            $data_formco['linkedin'] = $this->input->post('linkedin');
            $data_formco['nama_dm'] = $this->input->post('nama_dm');
            $data_formco['id_fungsional_dm'] = $this->input->post('id_fungsional_dm');
            $data_formco['id_jabatan_dm'] = $this->input->post('id_jabatan_dm');
            $data_formco['no_telp_dm'] = $this->input->post('no_telp_dm');
            $data_formco['nama_influ'] = $this->input->post('nama_influ');
            $data_formco['id_fungsional_influ'] = $this->input->post('id_fungsional_influ');
            $data_formco['id_jabatan_influ'] = $this->input->post('id_jabatan_influ');
            $data_formco['no_telp_influ'] = $this->input->post('no_telp_influ');
            $data_formco['id_user'] = $this->session->userdata('id_user');
            $data_formco['time'] = $this->today_datetime();

            $this->profil_pelanggan_m->update_profil_pelanggan($data_formco, $id_profil_pelanggan);
            $this->session->set_flashdata('sukses', 'Anda telah berhasil mengubah data customer.');
            redirect(base_url().'admin/customer/detil_data/'.$id_profil_pelanggan);
            
        }
    }

    public function delete_layanan_pelanggan($id_layanan_pelanggan, $id_profil_pelanggan){
        $this->layanan_pelanggan_m->delete_layanan_pelanggan($id_layanan_pelanggan);
        $this->session->set_flashdata('sukses', 'Anda telah berhasil menghapus data layanan.');
        redirect(base_url().'admin/customer/detil_data/'.$id_profil_pelanggan);
    }


    public function get_pic_pelanggan($id_pelanggan){
        if($id_pelanggan != ''){
            $datapic = $this->api_pic_pelanggan($id_pelanggan);

            echo json_encode($datapic);
        }
    }

    public function get_alamat_pelanggan($id_pelanggan){
        if($id_pelanggan != ''){
            $datapelanggan = $this->api_pelanggan();
            foreach ($datapelanggan as $row){
                if ($row['id'] == $id_pelanggan){
                    $alamat_pelanggan = $row['address'];
                }
            }
            echo $alamat_pelanggan;
        }
    }

    public function get_kabupaten($id_provinsi, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_provinsi != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            
            $data['list_kabupaten'] = $this->kabupaten_m->select_all_provinsi($id_provinsi);
            $this->load->view('admin/kabupaten_show_v', $data);
        }
    }

    public function get_kecamatan($id_kabupaten, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_kabupaten != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            $data['list_kecamatan'] = $this->kecamatan_m->select_all_kabupaten($id_kabupaten);
            $this->load->view('admin/kecamatan_show_v', $data);
         }
    }

    public function get_kelurahan($id_kecamatan, $mode_data = '', $id_profil_pelanggan = 0){
        if($id_kecamatan != ''){
            if($mode_data == 'edit'){
                $data['mode_data'] = $mode_data;
                $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_detil_profil_pelanggan($id_profil_pelanggan);
            }
            $data['list_kelurahan'] = $this->kelurahan_m->select_all_kecamatan($id_kecamatan);
            $this->load->view('admin/kelurahan_show_v', $data);
         }
    }

    public function get_kategori_layanan($id_jenis_layanan){
        if($id_jenis_layanan != ''){
            $data['list_kategori_layanan'] = $this->kategori_layanan_m->select_all_jenis($id_jenis_layanan);
            
            $this->load->view('admin/kategori_layanan_show_v', $data);
         }
    }

    public function get_layanan($id_kategori_layanan){
        if($id_kategori_layanan != ''){
            $data['list_layanan'] = $this->layanan_m->select_all_kategori($id_kategori_layanan);
            $this->load->view('admin/layanan_show_v', $data);
         }
    }

    public function all_data_mod(){
        $data['list_fungsional_pic'] = $this->fungsional_pic_m->select_all();
        $data['list_layanan_pelanggan'] = $this->layanan_pelanggan_m->select_all();
        $i=0;
        foreach($data['list_layanan_pelanggan'] as $row){
            $data['list_layanan_pelanggan'][$i]['tgl_mulai_new'] = $this->nama_bulan($data['list_layanan_pelanggan'][$i]['tgl_mulai']);
            $i++;
        }
        $data['list_jabatan_pic'] = $this->jabatan_pic_m->select_all(1);
        $data['list_kabupaten'] = $this->kabupaten_m->select_all();
        $data['list_kategori_layanan'] = $this->kategori_layanan_m->select_all();
        $data['list_kategori_berlangganan'] = $this->kategori_berlangganan_m->select_all();
        $data['list_kecamatan'] = $this->kecamatan_m->select_all();
        $data['list_kelurahan'] = $this->kelurahan_m->select_all(1);
		$data['list_layanan'] = $this->layanan_m->select_all();
        $data['list_provinsi'] = $this->provinsi_m->select_all(1);
        $data['list_profil_pelanggan'] = $this->profil_pelanggan_m->select_all();
        $data['list_sektor_industri'] = $this->sektor_industri_m->select_all();
        $data['list_user'] = $this->user_m->select_all();
        $data['list_yurisdiksi'] = $this->yurisdiksi_m->select_all();

        return $data;
    }

    private function api_pelanggan_sites() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.117:2020/getallclientsites');
        // http://192.168.0.117:2020/getclients
        // http://192.168.0.117:2020/getclientsbyname
        // http://192.168.0.117:2020/getallclientsites
        // http://192.168.0.117:2020/getclientsitesbyclientid/19 
        // 19 contoh client_id
        // dari mas puji
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        
        foreach ($obj as $obj_pelanggan){
            $api_pelanggan_sites[] = json_decode(json_encode($obj_pelanggan),true);
        }

        // var_dump($api_pelanggan_sites);

        return $api_pelanggan_sites;
    }

    public function api_pelanggan() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.117:2020/getclients');
        // http://192.168.0.117:2020/getclients
        // http://192.168.0.117:2020/getclientsbyname
        // http://192.168.0.117:2020/getallclientsites
        // http://192.168.0.117:2020/getclientsitesbyclientid/19 
        // 19 contoh client_id
        // dari mas puji
        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        
        foreach ($obj as $obj_pelanggan){
            $api_pelanggan[] = json_decode(json_encode($obj_pelanggan),true);
        }

        // var_dump($api_pelanggan);
        return $api_pelanggan;
    }

    public function api_pic_pelanggan($id_pelanggan) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.117:2020/getpicbyclientid/'.$id_pelanggan);

        $result = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($result);
        
        if (!empty($obj)){
            foreach ($obj as $obj_pelanggan){
                $api_pelanggan_pic[] = json_decode(json_encode($obj_pelanggan),true);
            }
    
    
            $i=0;
            foreach ($api_pelanggan_pic as $row){
                if ($i < 6){
                    $get_pelanggan_pic[] = $row;
                }
                $i++;
            }
            //digunakan utk mengambil 6 teratas. karena pada PadiApp, API mengecek semua FB tanpa segmen per FB. Jika ada dua fb maka ada 12 PIC, berlaku kelipatan. Sedangkan sistem hanya butuh 6.
            // var_dump($get_pelanggan_pic);

            return $get_pelanggan_pic;
        }
        
    }

    private function nama_bulan($tgl){
        $tanggal_full = date_create($tgl);
        $date = date_format($tanggal_full,'d M Y');
        $format = array(
            'Jan' => 'Januari',
            'Feb' => 'Februari',
            'Mar' => 'Maret',
            'Apr' => 'April',
            'May' => 'Mei',
            'Jun' => 'Juni',
            'Jul' => 'Juli',
            'Aug' => 'Agustus',
            'Sep' => 'September',
            'Oct' => 'Oktober',
            'Nov' => 'November',
            'Dec' => 'Desember'
        );
        $new_tgl = strtr(($date),$format);
        return $new_tgl;
    }
    
    private function today_datetime(){
        $datestring = '%Y-%m-%d %h:%i:%s';
        $time = time();
        $tanggal = mdate($datestring, $time);

        return $tanggal;
    }

    private function logged_in_cus(){
        if($this->session->userdata('logged_in')){
            $usr = $this->session->userdata('username');
            return isset($usr);
        }
    }

}
?>