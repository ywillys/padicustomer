<?php
ob_start();
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('date');
        $this->load->helper('security');
        $this->load->model('user_m','user_m');

        if($this->logged_in_cus()){
		}
		else {
			$this->session->set_flashdata('select', 'Periksa userlogin anda !');
			redirect(base_url());
		}
    }
    public function index(){
        $data = array(
            'css_file' => array(
                'assets/main.css'
            ),
            'css_link_plugin' => array(
                ''
            ),
            'js_link_plugin' => array(
               ''
            ),
            'js_file' => array(
                'assets/scripts/main.js'
            )
        );
        $data['template'] = "dashboard";
        $data['menu'] = "dashboard";
        $this->load->view('admin/templateroot_v', $data);
    }
    
    private function today_datetime(){
        $datestring = '%Y-%m-%d %h:%i:%s';
        $time = time();
        $tanggal = mdate($datestring, $time);

        return $tanggal;
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url().'adm_login');
    }

    public function logged_in_cus(){
        if($this->session->userdata('logged_in')){
            $usr = $this->session->userdata('username');
            return isset($usr);
        }
    }

}
?>