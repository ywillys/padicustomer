<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>PadiNet Marketing Area</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <?php
    echo link_tag('assets/main.css');
    ?>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-4">
            <div class="card-body card-login">
                <?php
                if ($this->session->flashdata('stop')!=NULL){
                    ?>
                    <div class="alert alert-warning">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Warning!</strong> <?php echo $this->session->flashdata('stop'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('select')!=NULL){
                    ?>
                    <div class="alert alert-danger">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Gagal!</strong> <?php echo $this->session->flashdata('select'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('logout')!=NULL){
                    ?>
                    <div class="alert alert-info">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Info.</strong> <?php echo $this->session->flashdata('logout'); ?>
                    </div>
                    <?php
                }
                ?>
                <h2 class="card-title">LOGIN</h2>
                <!-- <form class="needs-validation" novalidate> -->
                <?php echo form_open(base_url()."adm_login/submit_login", 'class="needs-validation" autocomplete="off" novalidate'); ?>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-user"></i></span>
                                </div>
                                <input type="text" class="form-control" id="validationCustomUsername" placeholder="Username" aria-describedby="inputGroupPrepend" name="username" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input username.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock"></i></span>
                                </div>
                                <input type="password" class="form-control" id="validationCustomUsername" placeholder="Password" aria-describedby="inputGroupPrepend" name="password" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input password.
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Login</button>
                <?php echo form_close(); ?>
            </div>
        </div>
        <div class="col-md-6">
        </div>
    </div>
</div>    
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict';
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();
</script>
<script src="<?php echo assets_url(); ?>assets/scripts/main.js"></script>
</body>
</html>


