<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-car icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Customer
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                </div> 
                <div class="page-title-actions">
                    <a href="<?php echo base_url(); ?>admin/customer/tambah_data">
                        <button type="button" data-toggle="tooltip" title="Tambah Data Baru" data-placement="bottom" class="btn-shadow mr-3 btn btn-info">
                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                <i class="fa fa-file fa-w-20"></i>
                            </span>
                            Add Data
                        </button>
                    </a>
                </div>
            </div>
        </div>            
        <div class="row">
        <div class="col-lg-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                <h5 class="card-title">List Customer</h5>
                    <?php
                    if(empty($list_profil_pelanggan)){
                        ?>
                        <div align="center">
                        <?php
                        echo "Data Masih Kosong.";
                        ?>
                        <div>
                        <?php    
                    } else {
                        ?>
                        <div class="table-responsive">
                            <table class="mb-0 table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama<br>Pelanggan</th>
                                    <th>Level<br>Perusahaan</th>
                                    <th>Sektor<br>Industri</th>
                                    <th>Kab / Kota</th>
                                    <th>Email</th>
                                    <th>Kategori<br>Berlangganan</th>
                                    <th>Decision<br>Maker</th>
                                    <th>Influencer</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach($list_profil_pelanggan as $row){
                                    ?>
                                    <tr>
                                        <th scope="row"><?php echo $i; ?></th>
                                        <td><?php echo $row['nama_pelanggan_update']; ?> </td>
                                        <td><?php echo $row['level_perusahaan']; ?></td>
                                        <td><?php echo $row['nama_sektor_industri']; ?></td>
                                        <td><?php echo $row['nama_kabupaten']; ?></td>
                                        <td><?php echo $row['email']; ?></td>
                                        <td><?php echo $row['nama_kategori_berlangganan']; ?>
                                            <?php 
                                            if( $row['status_layanan'] == 0){
                                                ?>
                                                <span data-toggle="tooltip" title="Layanan Belum Diisi !" style="color:orange">
                                                    <i class="fa fa-exclamation-triangle fa-w-20"></i>
                                                </span>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                        <td><?php echo $row['nama_dm']; ?></td>
                                        <td><?php echo $row['nama_influ']; ?></td>
                                        <td> 
                                            <a href="<?php echo base_url(); ?>admin/customer/detil_data/<?php echo $row['id_profil_pelanggan']; ?>">
                                            <button type="button" data-toggle="tooltip" title="Detil Data" data-placement="bottom" class="btn-shadow btn btn-dark">
                                                <i class="fa fa-address-card"></i>
                                            </button>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        </div>
    </div>  
</div>
