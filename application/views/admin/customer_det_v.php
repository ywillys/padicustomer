<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-display1 icon-gradient bg-premium-dark">
                        </i>
                    </div>
                    <div>Detail Customer
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                </div>
                
                <div class="page-title-actions">
                    <div class="d-inline-block dropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-warning">
                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                <i class="fa fa-business-time fa-w-20"></i>
                            </span>
                            Customer Menu
                        </button>
                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a href="<?php echo base_url();?>admin/customer" class="nav-link">
                                        <i class="nav-link-icon lnr-inbox"></i>
                                        <span class="btn-icon-wrapper pr-2">
                                            <i class="fa fa-arrow-left fa-w-20"></i>
                                        </span>
                                            Back To List Customer
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url();?>admin/customer/tambah_data" class="nav-link">
                                        <i class="nav-link-icon lnr-book"></i>
                                        <span class="btn-icon-wrapper pr-2 opacity-7">
                                            <i class="fa fa-file fa-w-20"></i>
                                        </span>
                                            Add New Customer
                                        </span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url();?>admin/customer/edit_data/<?php echo  $list_profil_pelanggan[0]['id_profil_pelanggan'] ?>" class="nav-link">
                                        <i class="nav-link-icon lnr-book"></i>
                                        <span class="btn-icon-wrapper pr-2 opacity-7">
                                            <i class="fa fa-magic fa-w-20"></i>
                                        </span>
                                            Edit Customer
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>   
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($this->session->flashdata('stop')!=NULL){
                    ?>
                    <div class="alert alert-warning">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Warning!</strong> <?php echo $this->session->flashdata('stop'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('select')!=NULL){
                    ?>
                    <div class="alert alert-danger">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Gagal!</strong> <?php echo $this->session->flashdata('select'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('logout')!=NULL){
                    ?>
                    <div class="alert alert-info">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Info.</strong> <?php echo $this->session->flashdata('logout'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('sukses')!=NULL){
                    ?>
                    <div class="alert alert-success">
                        <strong>Sukses.</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>    
        
        <?php echo form_open(base_url()."admin/customer/detil_data/".$list_profil_pelanggan[0]['id_profil_pelanggan'], 'class="needs-validation form-layanan-pelanggan" autocomplete="off" novalidate'); ?>
        <form class="needs-validation" novalidate>
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                    <div class="main-card mb-3 card">
                        <div class="card-header-tab card-header-tab-animation card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="header-icon lnr-gift icon-gradient bg-love-kiss"> </i><b><?php echo $list_profil_pelanggan[0]['nama_pelanggan_update']; ?></b></div>
                            <ul class="nav">
                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg8-0" class="active nav-link">Profil</a></li>
                                <li class="nav-item"><a data-toggle="tab" href="#tab-eg8-1" class="nav-link">Layanan</a></li>
                            </ul>
                        </div>
                            
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-eg8-0" role="tabpanel">
                                    <h5 class="card-title">Profil</h5>
                                    <div class="form-row"> 
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="mb-0 table" id="data-layanan-pelanggan">
                                                    <tr>
                                                        <td>Nama Pelanggan</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['nama_pelanggan_update']; ?></td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Alamat</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['alamat_pelanggan_update']; ?></td>
                                                        <td>
                                                        </td>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kelurahan</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['nama_kelurahan']; ?></td>
                                                        <td>Kecamatan</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['nama_kecamatan']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kota / Kab</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['nama_kabupaten']; ?></td>
                                                        <td>Provinsi</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['nama_provinsi']; ?></td>
                                                    </tr>
                                                </table>
                                            </div>    
                                        </div>
                                    </div>
                                    <br/>
                                    <h5 class="card-title">Media Sosial</h5>
                                    <div class="form-row"> 
                                        <div class="col-md-12">
                                            <div class="table-responsive">
                                                <table class="mb-0 table" id="data-layanan-pelanggan">
                                                    <tr>
                                                        <td>Email</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['email']; ?></td>
                                                        <td>LinkedIn</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['linkedin']; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Instagram</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['instagram']; ?></td>
                                                        <td>Facebook</td>
                                                        <td>:&nbsp;&nbsp;<?php echo $list_profil_pelanggan[0]['facebook']; ?></td>
                                                    </tr>
                                                </table>
                                            </div>    
                                        </div>
                                    </div>
                                    <br/><br/>
                                    <h5 class="card-title">PIC</h5>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            
                                            <div class="table-responsive">
                                                <table class="mb-0 table" id="data-layanan-pelanggan">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Role</th>
                                                        <th>Nama PIC</th>
                                                        <th>No Telp</th>
                                                        <th>Email</th>
                                                        <th>Hobi</th>
                                                        <th>Agama</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $i = 1;
                                                        foreach($list_pic_pelanggan as $row){
                                                        ?>
                                                            <tr>
                                                                <td><?php echo $i?></td>
                                                                <td>
                                                                    <?php 
                                                                    if ($row['role'] == 'support') echo "Support";
                                                                    else if ($row['role'] == 'billing') echo "Billing";
                                                                    else if ($row['role'] == 'adm') echo "Administrasi";
                                                                    else if ($row['role'] == 'subscriber') echo "Penanggung Jawab";
                                                                    else if ($row['role'] == 'resp') echo "Pemohon";
                                                                    ?>
                                                                </td>
                                                                <td><?php echo $row['nama_pic']?></td>
                                                                <td><?php echo $row['no_telp']?></td>
                                                                <td><?php echo $row['email']?></td>
                                                                <td><?php echo $row['hobi']?></td>
                                                                <td><?php echo $row['agama']?></td>
                                                            </tr> 
                                                        <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </tbody>
                                                
                                                </table>
                                            </div>   
                                        </div>
                                    </div>
                                </div>    
                                    
                                    
                                <div class="tab-pane" id="tab-eg8-1" role="tabpanel">
                                    <div align="center">
                                        <h5 class="card-title">Layanan</h5>
                                    </div>
                                    <div align="center">
                                        <button type="button" data-toggle="tooltip" title="Tambah Data Layanan" data-placement="bottom" class="btn-shadow btn btn-info btn-add-layanan">
                                        <span class="btn-icon-wrapper pr-2 ">
                                            <i class="fa fa-file fa-w-20"></i>
                                        </span>
                                        Add Data
                                        </button>
                                    </div>
                                    <div class="form-layanan">
                                        <h5 class="card-title"><i>Input Data</i></h5> 
                                        <div class="form-row">
                                            <div class="col-md-4">    
                                                <div class="position-relative form-group">
                                                    <label for="id_jenis_layanan" class="label-form-customer">Jenis Layanan</label>
                                                    <select name="id_jenis_layanan" id="id_jenis_layanan" class="form-control selectjenislayanan" required>
                                                        <option val=""></option>
                                                        <?php 
                                                        foreach ($list_jenis_layanan as $rowjl){
                                                        ?>
                                                            <option value = "<?php echo $rowjl["id_jenis_layanan"] ?>"><?php echo $rowjl["nama_jenis_layanan"] ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        Please choose data!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">    
                                                <div class="position-relative form-group">
                                                    <label for="id_kategori_layanan" class="label-form-customer">Kategori Layanan</label>
                                                    <select name="id_kategori_layanan" id="id_kategori_layanan" class="form-control selectkategorilayanan show-kategori-layanan" required>
                                                        <option val=""></option>
                                                        <!-- <?php 
                                                        foreach ($list_kategori_layanan as $rowkla){
                                                        ?>
                                                            <option value = "<?php echo $rowkla["id_kategori_layanan"] ?>"><?php echo $rowkla["nama_kategori_layanan"] ?></option>
                                                        <?php
                                                        }
                                                        ?> -->
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        Please choose data!
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">    
                                                <div class="position-relative form-group">
                                                    <label for="id_layanan" class="label-form-customer">Layanan</label>
                                                    <select name="id_layanan" id="id_layanan" class="form-control selectlayanan show-layanan" required>
                                                        <option val=""></option>
                                                    </select>
                                                    <div class="valid-feedback">
                                                        Looks good!
                                                    </div>
                                                    <div class="invalid-feedback">
                                                        Please choose data!
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">    
                                            <div class="col-md-4">    
                                                <div class="position-relative form-group">
                                                    <label for="tgl_mulai" class="label-form-customer">Tanggal Mulai Berlangganan</label>
                                                    <div class="input-group">
                                                        <input type="date" class="form-control" name="tgl_mulai" required> 
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                        <div class="invalid-feedback">
                                                            Please input data!
                                                        </div>
                                                    </div>       
                                                </div>
                                            </div>
                                            <div class="col-md-4">    
                                                <div class="position-relative form-group pelanggan-update">
                                                    <label for="nominal" class="label-form-customer">Nominal *</label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="nominal" required>
                                                        <div class="valid-feedback">
                                                            Looks good!
                                                        </div>
                                                        <div class="invalid-feedback">
                                                            Please input some data!
                                                        </div>
                                                    </div>          
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                            </div> 
                                        </div>
                                        <br />
                                        <div class="form-row">
                                            <div class="col-md-12">
                                                <button type="submit" data-toggle="tooltip" title="Simpan"
                                                class="btn-shadow btn btn-success">
                                                <span class="btn-icon-wrapper pr-2">
                                                    <i class="fa fa-check fa-w-20"></i>
                                                </span>
                                                Simpan
                                                </button>

                                                <button type="button" data-toggle="tooltip" title="Batal" class="btn-shadow btn btn-warning btn-cancel-layanan">
                                                <span class="btn-icon-wrapper pr-2 ">
                                                    <i class="fa fa-trash fa-w-20"></i>
                                                </span>
                                                </button>
                                            </div>   
                                        </div>
                                        
                                    </div>    
                                    <br />
                                    <div>
                                        <div class="form-row"> 
                                            <div class="col-md-12">
                                                <?php
                                                if(empty($list_det_layanan_pelanggan)){
                                                    ?>
                                                    <div align="center">
                                                    <?php
                                                    echo "Layanan Masih Kosong.";
                                                    ?>
                                                    <div>
                                                    <?php    
                                                } else {
                                                    ?>
                                                    <div class="table-responsive">
                                                        <table class="mb-0 table" id="data-layanan-pelanggan">
                                                            <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th>Nama Layanan</th>
                                                                <th>Jenis<br />Layanan</th>
                                                                <th>Kategori</th>
                                                                <th>Tgl Mulai<br />Berlangganan</th>
                                                                <th>Nominal</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php
                                                            $i = 1;
                                                            foreach($list_det_layanan_pelanggan as $row){
                                                                ?>
                                                                <tr id="<?php echo $row['id_layanan_pelanggan']; ?>">
                                                                    <th scope="row"><?php echo $i; ?></th>
                                                                    <td><?php echo $row['nama_layanan']; ?></td>
                                                                    <td><?php echo $row['nama_jenis_layanan']; ?></td>
                                                                    <td><?php echo $row['nama_kategori_layanan']; ?></td>
                                                                    <td><?php echo $row['tgl_mulai_new']; ?></td>
                                                                    <td><?php echo $row['nominal']; ?></td>
                                                                    <td>
                                                                        <button type="button" class="btn-shadow btn btn-dark" data-toggle="modal" data-target=".bd-example-modal-lg">
                                                                        <i class="fa fa-address-card"></i>
                                                                        Edit
                                                                        </button>
                                                                        <a href="<?php echo base_url() ?>admin/customer/delete_layanan_pelanggan/<?php echo $row['id_layanan_pelanggan']; ?>/<?php echo $list_profil_pelanggan[0]['id_profil_pelanggan']; ?>">
                                                                            <button type="button" data-toggle="tooltip" title="Delete" class="btn-shadow btn btn-warning btn-cancel-layanan">
                                                                            <span class="btn-icon-wrapper pr-2 ">
                                                                                <i class="fa fa-trash fa-w-20"></i>
                                                                            </span>
                                                                            </button>
                                                                        </a>
                                                                            
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                                $i++;
                                                            }
                                                            ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    
                                                    <?php
                                                }
                                                ?>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="d-block text-center card-footer">
                            <a href="javascript:void(0);" class="btn-wide btn btn-link">Link Button</a>
                            <a href="javascript:void(0);" class="btn-wide btn-shadow btn btn-danger">Delete</a>
                        </div> -->
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
