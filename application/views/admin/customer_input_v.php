
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-display1 icon-gradient bg-premium-dark">
                        </i>
                    </div>
                    <div>Form New Customer
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                    

                </div>
                <div class="page-title-actions">
                    <a href="<?php echo base_url();?>admin/customer">
                    <button type="button" data-toggle="tooltip" title="Kembali Ke List Data" data-placement="bottom" class="btn-shadow mr-3 btn btn-warning">
                        <span class="btn-icon-wrapper pr-2">
                            <i class="fa fa-arrow-left fa-w-20"></i>
                        </span>
                        Back to List
                    </button>
                    </a>
                </div>   
            </div>
        </div>            
        <!-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
            <li class="nav-item">
                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                    <span>Basic Info</span>
                </a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                    <span>Person In Charge</span>
                </a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                    <span>Custom Controls</span>
                </a>
            </li>
        </ul> -->
        
        <?php echo form_open(base_url()."admin/customer/tambah_data", 'class="needs-validation" autocomplete="off" novalidate'); ?>
        <form class="needs-validation" novalidate>
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Profile Pelanggan</h5>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="pilih_pelanggan" class="label-form-customer">Pilih Pelanggan (PadiApps) *</label>
                                        <select name="id_pelanggan" id="exampleSelect" class="form-control selectpelanggan" required>
                                            <option value=""></option>
                                            <?php 
                                            foreach ($api_pelanggan as $cus){
                                            ?>
                                                <option value = "<?php echo $cus["id"] ?>"><?php echo $cus["name"] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_pelanggan_update" class="label-form-customer">Nama Pelanggan Update *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-secondary btn-nama-pelanggan">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-copy"></i>
                                                    </span>
                                                    Copy
                                                </button>
                                            </div>
                                            <input type="text" class="form-control nama-pelanggan-update" name="nama_pelanggan_update" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>        
                                    </div>
                                </div> 
                                
                            </div>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                    <label for="alamat_pelanggan" class="label-form-customer">Alamat Pelanggan (PadiApps)</label>
                                    <textarea name="alamat_pelanggan_padiapps" id="alamat_pelanggan_padiapps" class="form-control text-area-alamat_pelanggan" disabled></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">    
                                    <div class="position-relative form-group">
                                        <label for="alamat_pelanggan_update" class="label-form-customer">Alamat Pelanggan Update *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-secondary btn-alamat-pelanggan">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-copy"></i>
                                                    </span>
                                                    Copy
                                                </button>
                                            </div>
                                            <textarea name="alamat_pelanggan_update" id="alamat_pelanggan_update" class="form-control alamat-pelanggan-update" required></textarea>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>      
                                    </div>
                                </div> 
                            </div>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_provinsi" class="label-form-customer">Provinsi *</label>
                                        <select name="id_provinsi" id="id_provinsi" class="form-control selectprovinsi" required>
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_provinsi as $rowprov){
                                            ?>
                                                <option value = "<?php echo $rowprov["id_provinsi"] ?>"><?php echo $rowprov["nama_provinsi"] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kabupaten" class="label-form-customer">Kota / Kab *</label>
                                        <select name="id_kabupaten" id="id_kabupaten" class="form-control selectkabupaten kabupaten-show" required>
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kecamatan" class="label-form-customer">Kecamatan *</label>
                                        <select name="id_kecamatan" id="id_kecamatan" class="form-control selectkecamatan kecamatan-show" required>
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kelurahan" class="label-form-customer">Kelurahan</label>
                                        <select name="id_kelurahan" id="id_kelurahan" class="form-control selectkelurahan kelurahan-show">
                                            <option></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_sektor_industri" class="label-form-customer">Sektor Industri *</label>
                                        <select name="id_sektor_industri" id="id_sektor_industri" class="form-control selectsektorindustri" required>
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_sektor_industri as $rowsek){
                                        ?>
                                            <option value = "<?php echo $rowsek["id_sektor_industri"] ?>"><?php echo $rowsek["nama_sektor_industri"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="level_perusahaan" class="label-form-customer">Level Perusahaan *</label>
                                        <select name="level_perusahaan" id="level_perusahaan" class="form-control selectlevelperusahaan" required>
                                            <option value=""></option>
                                            <option value="Besar">Besar</option>
                                            <option value="Menengah">Menengah</option>
                                            <option value="Kecil">Kecil</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_kategori_berlangganan" class="label-form-customer">Kategori Berlangganan *</label>
                                        <select name="id_kategori_berlangganan" id="id_kategori_berlangganan" class="form-control selectkategoriberlangganan" required>
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_kategori_berlangganan as $rowber){
                                        ?>
                                            <option value = "<?php echo $rowber["id_kategori_berlangganan"] ?>"><?php echo $rowber["nama_kategori_berlangganan"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 class="card-title pelanggan-update">Social Media</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email" class="label-form-customer">Email *</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" name="email" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>        
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="instagram" class="label-form-customer">Instagram</label>
                                        <div class="input-group">
                                            
                                            <input placeholder="http://" type="text" class="form-control" name="instagram">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="facebook" class="label-form-customer">Facebook</label>
                                        <div class="input-group">
                                            <input placeholder="http://" type="text" class="form-control" name="facebook">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>      
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="linkedin" class="label-form-customer">LinkedIn</label>
                                        <div class="input-group">
                                            <input placeholder="http://" type="text" class="form-control" name="linkedin">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                            </div>
                            <br /><br />
                            <!-- PIC -->
                            <h5 class="card-title pelanggan-update">Pemohon</h5> 
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_pemohon" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_pemohon" id="nama_pemohon">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_pemohon_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_pemohon_app" id="jabatan_pemohon_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_pemohon" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_pemohon" id="id_jabatan_pemohon" class="form-control selectjabatanpemohon">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_pemohon" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_pemohon" id="no_telp_pemohon">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_pemohon" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_pemohon" id="email_pemohon">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_pemohon" class="label-form-customer">Agama</label>
                                        <select name="agama_pemohon" id="agama_pemohon" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="hobi_pemohon" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="hobi_pemohon">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_pemohon" id="role_pemohon"> 
                            </div>
                            <h5 class="card-title pelanggan-update">Penanggung Jawab</h5> 
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_penanggung" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_penanggung" id="nama_penanggung">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_penanggung_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_penanggung_app" id="jabatan_penanggung_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_penanggung" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_penanggung" id="id_jabatan_penanggung" class="form-control selectjabatanpenanggung">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_penanggung" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_penanggung" id="no_telp_penanggung">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_penanggung" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_penanggung" id="email_penanggung">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_penanggung" class="label-form-customer">Agama</label>
                                        <select name="agama_penanggung" id="agama_penanggung" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="role_penanggung" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="role_penanggung">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_penanggung" id="role_penanggung">  
                            </div>
                            <h5 class="card-title pelanggan-update">Administrasi</h5> 
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_adm" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_adm" id="nama_adm">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_adm_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_adm_app" id="jabatan_adm_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_adm" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_adm" id="id_jabatan_adm" class="form-control selectjabatanadm">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_adm" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_adm" id="no_telp_adm">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_adm" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_adm" id="email_adm">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_adm" class="label-form-customer">Agama</label>
                                        <select name="agama_adm" id="agama_adm" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="role_adm" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="role_adm">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_adm" id="role_adm"> 
                            </div>
                            <h5 class="card-title pelanggan-update">Teknis</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_teknis" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_teknis" id="nama_teknis">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_teknis_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_teknis_app" id="jabatan_teknis_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_teknis" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_teknis" id="id_jabatan_teknis" class="form-control selectjabatanteknis">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_teknis" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_teknis" id="no_telp_teknis">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_teknis" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_teknis" id="email_teknis">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_teknis" class="label-form-customer">Agama</label>
                                        <select name="agama_teknis" id="agama_teknis" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="role_teknis" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="role_teknis">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_teknis" id="role_teknis"> 
                            </div>
                            <h5 class="card-title pelanggan-update">Billing</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_billing" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_billing" id="nama_billing">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_billing_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_billing_app" id="jabatan_billing_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_billing" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_billing" id="id_jabatan_billing" class="form-control selectjabatanbilling">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_billing" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_billing" id="no_telp_billing">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_billing" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_billing" id="email_billing">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_billing" class="label-form-customer">Agama</label>
                                        <select name="agama_billing" id="agama_billing" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="role_billing" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="role_billing">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_billing" id="role_billing"> 
                            </div>
                            <h5 class="card-title pelanggan-update">Support</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="nama_support" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_support" id="nama_support">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="jabatan_support_app" class="label-form-customer">Jabatan PadiApp</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="jabatan_support_app" id="jabatan_support_app" disabled>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_support" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_support" id="id_jabatan_support" class="form-control selectjabatansupport">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_jabatan_pic as $rowjab){
                                        ?>
                                            <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="no_telp_support" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_support" id="no_telp_support">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="email_support" class="label-form-customer">Email</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="email_support" id="email_support">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="agama_suppor" class="label-form-customer">Agama</label>
                                        <select name="agama_support" id="agama_support" class="form-control">
                                            <option value=""></option>
                                            <option value="Islam">Islam</option>
                                            <option value="Protestan">Protestan</option>
                                            <option value="Katolik">Katolik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Buddha">Buddha</option>
                                            <option value="Konghucu">Konghucu</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group pelanggan-update">
                                        <label for="role_support" class="label-form-customer">Hobi</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="role_support">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>  
                                <input type="hidden" class="form-control" name="role_support" id="role_support"> 
                            </div>
                            <br/><br/>
                            <button type="submit" class="mt-2 btn btn-primary pelanggan-update">
                            <span class="btn-icon-wrapper pr-2">
                                    <i class="fa fa-check fa-w-20"></i>
                            </span>
                            Save Data</button>
                            <br/><br/>
                            
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
