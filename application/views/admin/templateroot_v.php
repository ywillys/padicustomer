<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>
    <?php
        if ($template == "dashboard") {
            echo "Dashboard";
        }
        else if ($template == "customer") {
            echo "Customer";
        }
        else if ($template == "customertambahdata") {
            echo "New Customer";
        }
        else if ($template == "customerdetildata") {
            echo "Detail Customer";
        }
        else if ($template == "customereditdata") {
            echo "Edit Customer";
        }

        else if ($template == "filterinput") {
            echo "Customer Filter";
        }
        else if ($template == "filtershow") {
            echo "Result Customer Filter";
        }
    ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <!--
    =========================================================
    * ArchitectUI HTML Theme Dashboard - v1.0.0
    =========================================================
    * Product Page: https://dashboardpack.com
    * Copyright 2019 DashboardPack (https://dashboardpack.com)
    * Licensed under MIT (https://github.com/DashboardPack/architectui-html-theme-free/blob/master/LICENSE)
    =========================================================
    * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    -->
    <?php
    foreach($css_file as $path){
        echo link_tag($path);
    }
    foreach($css_link_plugin as $path){
        ?>
        <link href="<?php echo $path ?>" rel="stylesheet"/>
        <?php
    }
    ?>
</head>
<body>
      
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        
    <?php $this->load->view('admin/element/nav_topbar_v'); ?>      
    <div class="app-main">
        <?php $this->load->view('admin/element/nav_sidebar_v'); ?>
            <?php
            if ($template == "dashboard") {
                $this->load->view('admin/dashboard_v');
            }
            else if ($template == "customer") {
                $this->load->view('admin/customer_v');
            }
            else if ($template == "customertambahdata") {
                $this->load->view('admin/customer_input_v');
            }
            else if ($template == "customerdetildata") {
                $this->load->view('admin/customer_det_v');
            }
            else if ($template == "customereditdata") {
                $this->load->view('admin/customer_edit_v');
            }
            else if ($template == "filterinput") {
                $this->load->view('admin/filter_input_v');
            }
            else if ($template == "filtershow") {
                $this->load->view('admin/filter_show_v');
            }
            else {
                echo "View Page Not Found !";
            }
            ?>
            
    </div>
</div>
<?php 
if (isset($id_profil_pelanggan)){
    ?>
    <script>
        var ID_PROFIL_PELANGGAN = "<?php echo $id_profil_pelanggan; ?>";
    </script>
    <?php
}
?>
<script>
var BASEURLPHP = "<?php echo base_url(); ?>";
</script>


<?php
foreach($js_link_plugin as $path){
    ?>
    <script src="<?php echo $path ?>"></script>
    <?php
}
?>
<?php
foreach($js_file as $path){
    ?>
    <script src="<?php echo assets_url().$path ?>"></script>
    <?php
}

?>	
</body>
</html>
<?php if (isset($footer)) {
    foreach($footer as $viewfoot){
        $this->load->view($viewfoot);
    }
} ?>