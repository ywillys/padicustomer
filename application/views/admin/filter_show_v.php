<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-car icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Show Customer Filter
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    <a href="<?php echo base_url();?>admin/filterpelanggan">
                    <button type="button" data-toggle="tooltip" title="Kembali Ke Input Filter" data-placement="bottom" class="btn-shadow mr-3 btn btn-warning">
                        <span class="btn-icon-wrapper pr-2">
                            <i class="fa fa-arrow-left fa-w-20"></i>
                        </span>
                        Back to Input
                    </button>
                    </a>
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                    <h5 class="card-title">Filter Parameters</h5>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="mb-0 table table-borderless">
                                    <?php if(!empty($data_provinsi)){
                                        ?>
                                        <tr>
                                            <td>Provinsi </td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_provinsi[0]['nama_provinsi'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_kabupaten)){
                                        ?>
                                        <tr>
                                            <td>Kab / Kota</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_kabupaten[0]['nama_kabupaten'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_kecamatan)){
                                        ?>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_kecamatan[0]['nama_kecamatan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_kelurahan)){
                                        ?>
                                        <tr>
                                            <td>Kelurahan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_kelurahan[0]['nama_kelurahan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_kategori_berlangganan)){
                                        ?>
                                        <tr>
                                            <td>Kategori Berlangganan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_kategori_berlangganan[0]['nama_kategori_berlangganan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($level_perusahaan)){
                                        ?>
                                        <tr>
                                            <td>Level Perusahaan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $level_perusahaan ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_sektor_industri)){
                                        ?>
                                        <tr>
                                            <td>Sektor Industri</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_sektor_industri[0]['nama_sektor_industri'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_jenis_layanan)){
                                        ?>
                                        <tr>
                                            <td>Jenis Layanan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_jenis_layanan[0]['nama_jenis_layanan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_kategori_layanan)){
                                        ?>
                                        <tr>
                                            <td>Kategori Layanan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_kategori_layanan[0]['nama_kategori_layanan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($data_layanan)){
                                        ?>
                                        <tr>
                                            <td>Layanan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $data_layanan[0]['nama_layanan'] ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($tgl_start)){
                                        ?>
                                        <tr>
                                            <td>Tanggal Mulai Berlangganan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;<?php echo $tgl_start ?> - <?php echo $tgl_end ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php if(!empty($nominal_start)){
                                        ?>
                                        <tr>
                                            <td>Range Nominal Berlangganan</td>
                                            <td>:&nbsp;&nbsp;&nbsp;Rp. <?php echo $nominal_start ?> - Rp. <?php echo $nominal_end ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>              
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                    <h5 class="card-title">List Customer Result</h5>
                        <br />
                        <?php
                        if(empty($list_profil_pelanggan)){
                            ?>
                            <div align="center">
                            <?php
                            echo "<b><i>Tidak ada data berdasarkan pencarian tersebut.<br />Pastikan Data Layanan sudah diisi di setiap Customer.</i></b>";
                            ?>
                            <div>
                            <?php    
                        } else {
                            ?>
                            <div class="table-responsive">
                                <table class="mb-0 table">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama<br>Pelanggan</th>
                                        <th>Level<br>Perusahaan</th>
                                        <th>Sektor<br>Industri</th>
                                        <th>Kab / Kota</th>
                                        <th>Kec</th>
                                        <th>Email</th>
                                        <th>Kategori<br>Berlangganan</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 1;
                                    foreach($list_profil_pelanggan as $row){
                                        ?>
                                        <tr>
                                            <th scope="row"><?php echo $i; ?></th>
                                            <td><?php echo $row['nama_pelanggan_update']; ?></td>
                                            <td><?php echo $row['level_perusahaan']; ?></td>
                                            <td><?php echo $row['nama_sektor_industri']; ?></td>
                                            <td><?php echo $row['nama_kabupaten']; ?></td>
                                            <td><?php echo $row['nama_kecamatan']; ?></td>
                                            <td><?php echo $row['email']; ?></td>
                                            <td><?php echo $row['nama_kategori_berlangganan']; ?></td>
                                            <td> 
                                                <a target="_blank" href="<?php echo base_url(); ?>admin/customer/detil_data/<?php echo $row['id_profil_pelanggan']; ?>">
                                                <button type="button" data-toggle="tooltip" title="Detil Data (New Tab)" data-placement="bottom" class="btn-shadow btn btn-dark">
                                                    <i class="fa fa-address-card"></i>
                                                </button>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
