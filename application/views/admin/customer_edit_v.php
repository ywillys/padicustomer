
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-display1 icon-gradient bg-premium-dark">
                        </i>
                    </div>
                    <div>Form Edit Customer
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                    

                </div>
                <div class="page-title-actions">
                    <a href="<?php echo base_url();?>admin/customer">
                    <button type="button" data-toggle="tooltip" title="Kembali Ke List Data" data-placement="bottom" class="btn-shadow mr-3 btn btn-warning">
                        <span class="btn-icon-wrapper pr-2">
                            <i class="fa fa-arrow-left fa-w-20"></i>
                        </span>
                        Back to List
                    </button>
                    </a>
                </div>   
            </div>
        </div>            
        <!-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
            <li class="nav-item">
                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                    <span>Basic Info</span>
                </a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                    <span>Person In Charge</span>
                </a>
            </li>
            <li class="nav-item">
                <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                    <span>Custom Controls</span>
                </a>
            </li>
        </ul> -->
        
        <?php echo form_open(base_url()."admin/customer/edit_data/".$list_profil_pelanggan[0]['id_profil_pelanggan'], 'class="needs-validation" autocomplete="off" novalidate'); ?>
        <form class="needs-validation" novalidate>
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <h5 class="card-title">Profile Pelanggan</h5>
                            <div class="ujitampilan"></div>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                        <label for="pilih_pelanggan" class="label-form-customer">Pilih Pelanggan (PadiApps) *</label>
                                        <select name="id_pelanggan" id="exampleSelect" class="form-control selectpelanggan" required>
                                        <?php 
                                        foreach ($api_pelanggan as $cus){
                                            if($cus["id"] == $list_profil_pelanggan[0]['id_pelanggan']){
                                                ?>
                                                <option selected value = "<?php echo $cus["id"] ?>"><?php echo $cus["name"] ?></option>
                                                <?php
                                            } else {
                                            ?>
                                                <option value = "<?php echo $cus["id"] ?>"><?php echo $cus["name"] ?></option>
                                            <?php
                                            }
                                        
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">    
                                    <div class="position-relative form-group ">
                                        <label for="nama_pelanggan_update" class="label-form-customer">Nama Pelanggan Update *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-secondary btn-nama-pelanggan">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-copy"></i>
                                                    </span>
                                                    Copy
                                                </button>
                                            </div>
                                            <input type="text" class="form-control nama-pelanggan-update" name="nama_pelanggan_update" value="<?php echo $list_profil_pelanggan[0]['nama_pelanggan_update']; ?>" required>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>        
                                    </div>
                                </div> 
                                
                            </div>
                            <div class="form-row ">
                                <div class="col-md-6">
                                    <div class="position-relative form-group">
                                    <label for="alamat_pelanggan" class="label-form-customer">Alamat Pelanggan (PadiApps)</label>
                                    <textarea name="alamat_pelanggan_padiapps" id="alamat_pelanggan_padiapps" class="form-control text-area-alamat_pelanggan" disabled></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">    
                                    <div class="position-relative form-group">
                                        <label for="alamat_pelanggan_update" class="label-form-customer">Alamat Pelanggan Update *</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button type="button" class="btn btn-secondary btn-alamat-pelanggan">
                                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                                        <i class="fa fa-copy"></i>
                                                    </span>
                                                    Copy
                                                </button>
                                            </div>
                                            <textarea name="alamat_pelanggan_update" id="alamat_pelanggan_update" class="form-control alamat-pelanggan-update" required><?php echo $list_profil_pelanggan[0]['alamat_pelanggan_update']; ?></textarea>
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>      
                                    </div>
                                </div> 
                            </div>
                            <div class="form-row ">
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_provinsi" class="label-form-customer">Provinsi *</label>
                                        <select name="id_provinsi" id="id_provinsi" class="form-control selectprovinsi" required>
                                            <?php 
                                            foreach ($list_provinsi as $rowprov){
                                                if($rowprov["id_provinsi"] == $list_profil_pelanggan[0]['id_provinsi']){
                                                    ?>
                                                   <option selected value = "<?php echo $rowprov["id_provinsi"] ?>"><?php echo $rowprov["nama_provinsi"] ?></option>
                                                    <?php
                                                } else {
                                            ?>
                                                <option value = "<?php echo $rowprov["id_provinsi"] ?>"><?php echo $rowprov["nama_provinsi"] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kabupaten" class="label-form-customer">Kota / Kab *</label>
                                        <select name="id_kabupaten" id="id_kabupaten" class="form-control selectkabupaten kabupaten-show" required>
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kecamatan" class="label-form-customer">Kecamatan *</label>
                                        <select name="id_kecamatan" id="id_kecamatan" class="form-control selectkecamatan kecamatan-show" required>
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kelurahan" class="label-form-customer">Kelurahan</label>
                                        <select name="id_kelurahan" id="id_kelurahan" class="form-control selectkelurahan kelurahan-show">
                                            <option></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="form-row ">
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_sektor_industri" class="label-form-customer">Sektor Industri *</label>
                                        <select name="id_sektor_industri" id="id_sektor_industri" class="form-control selectsektorindustri" required>
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_sektor_industri as $rowsek){
                                            if($rowsek["id_sektor_industri"] == $list_profil_pelanggan[0]['id_sektor_industri']){
                                                ?>
                                                <option selected value = "<?php echo $rowsek["id_sektor_industri"] ?>"><?php echo $rowsek["nama_sektor_industri"] ?></option>
                                                <?php
                                            } else {
                                        ?>
                                            <option value = "<?php echo $rowsek["id_sektor_industri"] ?>"><?php echo $rowsek["nama_sektor_industri"] ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="level_perusahaan" class="label-form-customer">Level Perusahaan *</label>
                                        <select name="level_perusahaan" id="level_perusahaan" class="form-control selectlevelperusahaan" required>
                                            <?php
                                            if ($list_profil_pelanggan[0]['level_perusahaan'] == "Besar"){
                                                ?>
                                                <option selected value="Besar">Besar</option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="Besar">Besar</option>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($list_profil_pelanggan[0]['level_perusahaan'] == "Menengah"){
                                                ?>
                                                <option selected value="Menengah">Menengah</option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="Menengah">Menengah</option>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($list_profil_pelanggan[0]['level_perusahaan'] == "Kecil"){
                                                ?>
                                                <option selected value="Kecil">Kecil</option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="Kecil">Kecil</option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_kategori_berlangganan" class="label-form-customer">Kategori Berlangganan *</label>
                                        
                                        <select name="id_kategori_berlangganan" id="id_kategori_berlangganan" class="form-control selectkategoriberlangganan" required>
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_kategori_berlangganan as $rowber){
                                            if($rowber["id_kategori_berlangganan"] == $list_profil_pelanggan[0]['id_kategori_berlangganan']){
                                                ?>
                                                <option selected value = "<?php echo $rowber["id_kategori_berlangganan"] ?>"><?php echo $rowber["nama_kategori_berlangganan"] ?></option>
                                                <?php
                                            } else {
                                        ?>
                                            <option value = "<?php echo $rowber["id_kategori_berlangganan"] ?>"><?php echo $rowber["nama_kategori_berlangganan"] ?></option>
                                        <?php
                                            }
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h5 class="card-title ">Social Media</h5>
                            <div class="form-row ">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="email" class="label-form-customer">Email *</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" name="email" required value="<?php echo $list_profil_pelanggan[0]['email'];  ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input some data!
                                            </div>
                                        </div>        
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="instagram" class="label-form-customer">Instagram</label>
                                        <div class="input-group">
                                            
                                            <input placeholder="http://" type="text" class="form-control" name="instagram" value="<?php echo $list_profil_pelanggan[0]['instagram'];  ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="facebook" class="label-form-customer">Facebook</label>
                                        <div class="input-group">
                                            <input placeholder="http://" type="text" class="form-control" name="facebook" value="<?php echo $list_profil_pelanggan[0]['facebook']; ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>      
                                    </div>
                                </div> 
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="linkedin" class="label-form-customer">LinkedIn</label>
                                        <div class="input-group">
                                            <input placeholder="http://" type="text" class="form-control" name="linkedin" value="<?php echo $list_profil_pelanggan[0]['linkedin'];  ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                            </div>
                            <h5 class="card-title ">Decision Maker</h5> 
                            <div class="form-row ">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="nama_dm" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_dm" value="<?php echo $list_profil_pelanggan[0]['nama_dm']; ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_fungsional_dm" class="label-form-customer">Fungsional</label>
                                        <select name="id_fungsional_dm" id="id_fungsional_dm" class="form-control selectfungsionaldm">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_fungsional_pic as $rowfun){
                                                if($rowfun["id_fungsional_pic"] == $list_profil_pelanggan[0]['id_fungsional_dm']){
                                                    ?>
                                                    <<option selected value = "<?php echo $rowfun["id_fungsional_pic"] ?>"><?php echo $rowfun["nama_fungsional_pic"] ?>
                                                    <?php
                                                } else {
                                            ?>
                                                <option value = "<?php echo $rowfun["id_fungsional_pic"] ?>"><?php echo $rowfun["nama_fungsional_pic"] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_dm" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_dm" id="id_jabatan_dm" class="form-control selectjabatandm jabatandm-show">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_jabatan_pic as $rowjab){
                                                if($rowjab["id_jabatan_pic"] == $list_profil_pelanggan[0]['id_jabatan_dm']){
                                                    ?>
                                                    <option selected value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                                    <?php
                                                } else {
                                            ?>
                                                <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="no_telp_dm" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_dm" value="<?php echo $list_profil_pelanggan[0]['no_telp_dm']; ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                            </div>
                            <h5 class="card-title ">Influencer</h5> 
                            <div class="form-row ">
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="nama_influ" class="label-form-customer">Nama</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nama_influ" value="<?php echo $list_profil_pelanggan[0]['nama_influ']; ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_fungsional_influ" class="label-form-customer">Fungsional</label>
                                        <select name="id_fungsional_influ" id="id_fungsional_influ" class="form-control selectfungsionalinflu">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_fungsional_pic as $rowfun){
                                                if($rowfun["id_fungsional_pic"] == $list_profil_pelanggan[0]['id_fungsional_influ']){
                                                    ?>
                                                    <option selected value = "<?php echo $rowfun["id_fungsional_pic"] ?>"><?php echo $rowfun["nama_fungsional_pic"] ?>
                                                    <?php
                                                } else {
                                            ?>
                                                <option value = "<?php echo $rowfun["id_fungsional_pic"] ?>"><?php echo $rowfun["nama_fungsional_pic"] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_jabatan_influ" class="label-form-customer">Jabatan</label>
                                        <select name="id_jabatan_influ" id="id_jabatan_influ" class="form-control selectjabataninflu jabataninflu-show">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_jabatan_pic as $rowjab){
                                                if($rowjab["id_fungsional_pic"] == $list_profil_pelanggan[0]['id_jabatan_influ']){
                                                    ?>
                                                    <option selected value = "<?php echo $rowjab["id_fungsional_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                                    <?php
                                                } else {
                                            ?>
                                                <option value = "<?php echo $rowjab["id_jabatan_pic"] ?>"><?php echo $rowjab["nama_jabatan_pic"] ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">    
                                    <div class="position-relative form-group ">
                                        <label for="no_telp_influ" class="label-form-customer">No Telpon</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="no_telp_influ" value="<?php echo $list_profil_pelanggan[0]['no_telp_influ']; ?>">
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div> 
                            </div>
                            <br/><br/>
                            <button type="submit" class="mt-2 btn btn-primary ">
                            <span class="btn-icon-wrapper pr-2">
                                    <i class="fa fa-check fa-w-20"></i>
                            </span>
                            Update Data</button>
                            <br/><br/>
                            
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
