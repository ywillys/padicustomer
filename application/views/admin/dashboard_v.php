<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-car icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Dashboard
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                </div> 
            </div>
        </div>            
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($this->session->flashdata('stop')!=NULL){
                    ?>
                    <div class="alert alert-warning">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Warning!</strong> <?php echo $this->session->flashdata('stop'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('select')!=NULL){
                    ?>
                    <div class="alert alert-danger">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Gagal!</strong> <?php echo $this->session->flashdata('select'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('logout')!=NULL){
                    ?>
                    <div class="alert alert-info">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Info.</strong> <?php echo $this->session->flashdata('logout'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('sukses')!=NULL){
                    ?>
                    <div class="alert alert-success">
                        <strong>Sukses.</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="mb-3 card">
                    <div class="card-header-tab card-header-tab-animation card-header">
                        <div class="card-header-title">
                            <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                            Dashboard
                        </div>
                    </div>
                    <div class="card-body">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>

