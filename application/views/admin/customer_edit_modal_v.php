<!-- Large modal -->
<?php echo form_open(base_url()."admin/customer/edit_data_layanan/", 'class="needs-validation form-layanan-pelanggan" autocomplete="off" novalidate'); ?>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Layanan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-md-4">    
                        <div class="position-relative form-group">
                            <label for="id_jenis_layanan" class="label-form-customer">Jenis Layanan</label>
                            <select name="id_jenis_layanan_edit" id="id_jenis_layanan_edit" class="form-control selectjenislayananedit" required>
                                <option val=""></option>
                                <?php 
                                foreach ($list_jenis_layanan as $rowjl){
                                ?>
                                    <option value = "<?php echo $rowjl["id_jenis_layanan"] ?>"><?php echo $rowjl["nama_jenis_layanan"] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please choose data!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">    
                        <div class="position-relative form-group">
                            <label for="id_kategori_layanan" class="label-form-customer">Kategori Layanan</label>
                            <select name="id_kategori_layanan_edit" id="id_kategori_layanan_edit" class="form-control selectkategorilayananedit show-kategori-layanan-edit" required>
                                <option val=""></option>
                                <!-- <?php 
                                foreach ($list_kategori_layanan as $rowkla){
                                ?>
                                    <option value = "<?php echo $rowkla["id_kategori_layanan"] ?>"><?php echo $rowkla["nama_kategori_layanan"] ?></option>
                                <?php
                                }
                                ?> -->
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please choose data!
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">    
                        <div class="position-relative form-group">
                            <label for="id_layanan" class="label-form-customer">Layanan</label>
                            <select name="id_layanan_edit" id="id_layanan_edit" class="form-control selectlayananedit show-layanan-edit" required>
                                <option val=""></option>
                            </select>
                            <div class="valid-feedback">
                                Looks good!
                            </div>
                            <div class="invalid-feedback">
                                Please choose data!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">    
                    <div class="col-md-4">    
                        <div class="position-relative form-group">
                            <label for="tgl_mulai" class="label-form-customer">Tanggal Mulai Berlangganan</label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="tgl_mulai_edit" required> 
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input data!
                                </div>
                            </div>       
                        </div>
                    </div>
                    <div class="col-md-4">    
                        <div class="position-relative form-group pelanggan-update">
                            <label for="nominal" class="label-form-customer">Nominal *</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="nominal_edit" required>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input some data!
                                </div>
                            </div>          
                        </div>
                    </div> 
                    <div class="col-md-4">
                    </div> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>