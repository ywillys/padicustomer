
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-display1 icon-gradient bg-premium-dark">
                        </i>
                    </div>
                    <div>Customer Filter
                        <div class="page-title-subheading">Padi Customer.
                        </div>
                    </div>
                </div>
                
            </div>
        </div>  
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($this->session->flashdata('stop')!=NULL){
                    ?>
                    <div class="alert alert-warning">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Warning!</strong> <?php echo $this->session->flashdata('stop'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('select')!=NULL){
                    ?>
                    <div class="alert alert-danger">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Gagal!</strong> <?php echo $this->session->flashdata('select'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('logout')!=NULL){
                    ?>
                    <div class="alert alert-info">
                        <!--                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>-->
                        <strong>Info.</strong> <?php echo $this->session->flashdata('logout'); ?>
                    </div>
                    <?php
                }
                ?>
                <?php
                if ($this->session->flashdata('sukses')!=NULL){
                    ?>
                    <div class="alert alert-success">
                        <strong>Sukses.</strong> <?php echo $this->session->flashdata('sukses'); ?>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>              
        
        <?php echo form_open(base_url()."admin/filterpelanggan/", 'class="needs-validation" autocomplete="off" novalidate'); ?>
        <form class="needs-validation" novalidate>
            <div class="tab-content">
                <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                        <input type="hidden" class="form-control" name="input_status" value="filterdatapelanggan" required>
                            <h5 class="card-title">Filter Data Customer</h5>
                            <br />
                            <div class="form-row pelanggan-update">
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_provinsi" class="label-form-customer">Provinsi</label>
                                        <select name="id_provinsi" id="id_provinsi" class="form-control selectprovinsi">
                                            <option value=""></option>
                                            <?php 
                                            foreach ($list_provinsi as $rowprov){
                                            ?>
                                                <option value = "<?php echo $rowprov["id_provinsi"] ?>"><?php echo $rowprov["nama_provinsi"] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>   
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kabupaten" class="label-form-customer">Kota / Kab</label>
                                        <select name="id_kabupaten" id="id_kabupaten" class="form-control selectkabupaten kabupaten-show">
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kecamatan" class="label-form-customer">Kecamatan</label>
                                        <select name="id_kecamatan" id="id_kecamatan" class="form-control selectkecamatan kecamatan-show">
                                            <option value=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label for="id_kelurahan" class="label-form-customer">Kelurahan</label>
                                        <select name="id_kelurahan" id="id_kelurahan" class="form-control selectkelurahan kelurahan-show">
                                            <option></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_sektor_industri" class="label-form-customer">Sektor Industri</label>
                                        <select name="id_sektor_industri" id="id_sektor_industri" class="form-control selectsektorindustri">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_sektor_industri as $rowsek){
                                        ?>
                                            <option value = "<?php echo $rowsek["id_sektor_industri"] ?>"><?php echo $rowsek["nama_sektor_industri"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="level_perusahaan" class="label-form-customer">Level Perusahaan</label>
                                        <select name="level_perusahaan" id="level_perusahaan" class="form-control selectlevelperusahaan">
                                            <option value=""></option>
                                            <option value="Besar">Besar</option>
                                            <option value="Menengah">Menengah</option>
                                            <option value="Kecil">Kecil</option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="position-relative form-group">
                                        <label for="id_kategori_berlangganan" class="label-form-customer">Kategori Berlangganan</label>
                                        <select name="id_kategori_berlangganan" id="id_kategori_berlangganan" class="form-control selectkategoriberlangganan">
                                        <option value=""></option>
                                        <?php 
                                        foreach ($list_kategori_berlangganan as $rowber){
                                        ?>
                                            <option value = "<?php echo $rowber["id_kategori_berlangganan"] ?>"><?php echo $rowber["nama_kategori_berlangganan"] ?></option>
                                        <?php
                                        }
                                        ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="id_jenis_layanan" class="label-form-customer">Jenis Layanan</label>
                                        <select name="id_jenis_layanan" id="id_jenis_layanan" class="form-control selectjenislayanan">
                                            <option val=""></option>
                                            <?php 
                                            foreach ($list_jenis_layanan as $rowjl){
                                            ?>
                                                <option value = "<?php echo $rowjl["id_jenis_layanan"] ?>"><?php echo $rowjl["nama_jenis_layanan"] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="id_kategori_layanan" class="label-form-customer">Kategori Layanan</label>
                                        <select name="id_kategori_layanan" id="id_kategori_layanan" class="form-control selectkategorilayanan show-kategori-layanan">
                                            <option val=""></option>
                                            <!-- <?php 
                                            foreach ($list_kategori_layanan as $rowkla){
                                            ?>
                                                <option value = "<?php echo $rowkla["id_kategori_layanan"] ?>"><?php echo $rowkla["nama_kategori_layanan"] ?></option>
                                            <?php
                                            }
                                            ?> -->
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="id_layanan" class="label-form-customer">Layanan</label>
                                        <select name="id_layanan" id="id_layanan" class="form-control selectlayanan show-layanan">
                                            <option val=""></option>
                                        </select>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                        <div class="invalid-feedback">
                                            Please choose data!
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <h5 class="card-title">Range Tanggal Join</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="tgl_mulai" class="label-form-customer">Sejak Tanggal</label>
                                        <div class="input-group">
                                            <input type="date" class="form-control" name="tgl_start"> 
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="tgl_mulai" class="label-form-customer">Sampai Tanggal</label>
                                        <div class="input-group">
                                            <input type="date" class="form-control" name="tgl_end"> 
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                    
                                </div>
                            </div>
                            <br />
                            <h5 class="card-title">Range Nominal Berlangganan</h5>
                            <div class="form-row pelanggan-update">
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="tgl_mulai" class="label-form-customer">Mulai Nominal Rp.</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="nominal_start"> 
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                </div>
                                <div class="col-md-4">    
                                    <div class="position-relative form-group">
                                        <label for="tgl_mulai" class="label-form-customer">Sampai Nominal Rp.</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" name="nominal_end"> 
                                            <div class="valid-feedback">
                                                Looks good!
                                            </div>
                                            <div class="invalid-feedback">
                                                Please input data!
                                            </div>
                                        </div>       
                                    </div>
                                    
                                </div>
                            </div>
                            <br/><br/>
                            <button type="submit" class="mt-2 btn btn-primary pelanggan-update">
                            <span class="btn-icon-wrapper pr-2">
                                    <i class="fa fa-check fa-w-20"></i>
                            </span>
                            Find Data</button>
                            <br/><br/>
                            
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
