<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_layanan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_kategori_layanan(){
        $query = $this->db->get('m_kategori_layanan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_kategori_layanan($data){
        $this->db->insert('m_kategori_layanan', $data);
    }
    function select_detil_kategori_layanan($id){
        $query = $this->db->get_where('m_kategori_layanan', array('id_kategori_layanan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_kategori_layanan($id, $data) {
        $this->db->where('id_kategori_layanan', $id);
        return $this->db->update('m_kategori_layanan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_kategori_layanan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_kategori_layanan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_kategori_layanan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_kategori_layanan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_kategori_layanan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }

    function select_all_jenis($id_jenis_layanan){
        $query = $this->db->get_where('m_kategori_layanan', array('id_jenis_layanan' => $id_jenis_layanan));
        $result_array = $query->result_array();

        
        return $result_array;
    }


    function jum_kategori_layanan(){
        $query = $this->db->get('m_kategori_layanan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_kategori_layanan_search($where){
        $sql = " SELECT * FROM m_kategori_layanan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_kategori_layanan($id) {
        $this->db->where('id_kategori_layanan', $id);
        return $this->db->delete('m_kategori_layanan');
    }
}