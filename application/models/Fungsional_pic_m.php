<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fungsional_pic_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_layanan_pelanggan(){
        $query = $this->db->get('m_fungsional_pic');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_fungsional_pic($data){
        $this->db->insert('m_fungsional_pic', $data);
    }
    function select_detil_fungsional_pic($id){
        $query = $this->db->get_where('m_fungsional_pic', array('id_fungsional_pic' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_fungsional_pic($id, $data) {
        $this->db->where('id_fungsional_pic', $id);
        return $this->db->update('m_fungsional_pic', $data);
    }

    function select_all(){
        $query = $this->db->get('m_fungsional_pic');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_fungsional_pic', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_fungsional_pic
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_fungsional_pic', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_fungsional_pic
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_fungsional_pic(){
        $query = $this->db->get('m_fungsional_pic');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_fungsional_pic_search($where){
        $sql = " SELECT * FROM m_fungsional_pic
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_fungsional_pic($id) {
        $this->db->where('id_fungsional_pic', $id);
        return $this->db->delete('m_fungsional_pic');
    }
}