<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sektor_industri_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_sektor_industri(){
        $query = $this->db->get('m_sektor_industri');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_sektor_industri($data){
        $this->db->insert('m_sektor_industri', $data);
    }
    function select_detil_sektor_industri($id){
        $query = $this->db->get_where('m_sektor_industri', array('id_sektor_industri' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_sektor_industri($id, $data) {
        $this->db->where('id_sektor_industri', $id);
        return $this->db->update('m_sektor_industri', $data);
    }

    function select_all(){
        $query = $this->db->get('m_sektor_industri');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_sektor_industri', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_sektor_industri
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_sektor_industri', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_sektor_industri
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_sektor_industri(){
        $query = $this->db->get('m_sektor_industri');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_sektor_industri_search($where){
        $sql = " SELECT * FROM m_sektor_industri
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_sektor_industri($id) {
        $this->db->where('id_sektor_industri', $id);
        return $this->db->delete('m_sektor_industri');
    }
}