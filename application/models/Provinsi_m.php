<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provinsi_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_provinsi(){
        $query = $this->db->get('m_provinsi');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_provinsi($data){
        $this->db->insert('m_provinsi', $data);
    }
    function select_detil_provinsi($id){
        $query = $this->db->get_where('m_provinsi', array('id_provinsi' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_provinsi($id, $data) {
        $this->db->where('id_provinsi', $id);
        return $this->db->update('m_provinsi', $data);
    }

    function select_all(){
        $query = $this->db->get('m_provinsi');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_provinsi', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_provinsi
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_provinsi', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_provinsi
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_provinsi(){
        $query = $this->db->get('m_provinsi');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_provinsi_search($where){
        $sql = " SELECT * FROM m_provinsi
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_provinsi($id) {
        $this->db->where('id_provinsi', $id);
        return $this->db->delete('m_provinsi');
    }
}