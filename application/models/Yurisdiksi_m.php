<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Yurisdiksi_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_yurisdiksi(){
        $query = $this->db->get('m_yurisdiksi');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_yurisdiksi($data){
        $this->db->insert('m_yurisdiksi', $data);
    }
    function select_detil_yurisdiksi($id){
        $query = $this->db->get_where('m_yurisdiksi', array('id_yurisdiksi' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_yurisdiksi($id, $data) {
        $this->db->where('id_yurisdiksi', $id);
        return $this->db->update('m_yurisdiksi', $data);
    }

    function select_all(){
        $query = $this->db->get('m_yurisdiksi');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_yurisdiksi', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_yurisdiksi
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_yurisdiksi', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_yurisdiksi
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_yurisdiksi(){
        $query = $this->db->get('m_yurisdiksi');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_yurisdiksi_search($where){
        $sql = " SELECT * FROM m_yurisdiksi
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_yurisdiksi($id) {
        $this->db->where('id_yurisdiksi', $id);
        return $this->db->delete('m_yurisdiksi');
    }
}