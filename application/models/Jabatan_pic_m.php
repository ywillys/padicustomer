<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_pic_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_layanan_pelanggan(){
        $query = $this->db->get('m_jabatan_pic');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_jabatan_pic($data){
        $this->db->insert('m_jabatan_pic', $data);
    }
    function select_detil_jabatan_pic($id){
        $query = $this->db->get_where('m_jabatan_pic', array('id_jabatan_pic' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_jabatan_pic($id, $data) {
        $this->db->where('id_jabatan_pic', $id);
        return $this->db->update('m_jabatan_pic', $data);
    }

    function select_all(){
        $query = $this->db->get('m_jabatan_pic');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_fungsional($id_fungsional){
        $query = $this->db->get_where('m_jabatan_pic', array('id_fungsional_pic' => $id_fungsional));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_jabatan_pic', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_jabatan_pic
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_jabatan_pic', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_jabatan_pic
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_jabatan_pic(){
        $query = $this->db->get('m_jabatan_pic');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_jabatan_pic_search($where){
        $sql = " SELECT * FROM m_jabatan_pic
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_jabatan_pic($id) {
        $this->db->where('id_jabatan_pic', $id);
        return $this->db->delete('m_jabatan_pic');
    }
}