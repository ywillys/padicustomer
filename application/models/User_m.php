<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	function get_user($username){
		$sql = 'SELECT * FROM m_user WHERE username = "'.$username.'"';
		$query = $this->db->query($sql);
		$hasil['result'] = $query->result_array();
		$hasil['jumlah'] = $query->num_rows();
		return $hasil;
	}
	function select_jum_user(){
		$query = $this->db->get('m_user');
		$hasil['result'] = $query->result_array();
		$hasil['jumlah'] = $query->num_rows();
		return $hasil;
	}
	function insert_user($data){
		$this->db->insert('m_user', $data);
	}
	function select_detil_user($id){
		$query = $this->db->get_where('m_user', array('id_user' => $id));
		$result_array = $query->result_array();

		return $result_array;
	}
	function update_user($id, $data) {
		$this->db->where('id_user', $id);
		return $this->db->update('m_user', $data);
	}

	function select_all(){
		$query = $this->db->get_where('m_user', array('level !=' => 'super'));//namatabel
		$result_array = $query->result_array();

		return $result_array;
	}
	function select_all_active(){
		$query = $this->db->get_where('m_user', array('status_aktif' => 1, 'level !=' => 'super'));//namatabel
		$result_array = $query->result_array();

		return $result_array;
	}

	function select_all_search($where, $orderby, $ordertype){
		$sql = " SELECT * FROM m_user
             ".$where."
              ".$orderby." ".$ordertype."
        ";
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;

	}
	function select_allpaging($limit,$offset){
		$query = $this->db->get_where('m_user', array('level !=' => 'super'), $limit, $offset);//namatabel
		$result_array = $query->result_array();

		return $result_array;
	}
	function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
		$sql = " SELECT * FROM m_user
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
		$query = $this->db->query($sql);
		$result = $query->result_array();

		return $result;

	}
	function jum_user(){
		$query = $this->db->get_where('m_user', array('level !=' => 'super'));//namatabel
		$jum = $query->num_rows();
		return $jum;
	}
	function jum_user_search($where){
		$sql = " SELECT * FROM m_user
              ".$where."
        ";
		$query = $this->db->query($sql);
		$jum = $query->num_rows();
		return $jum;
	}

	function delete_user($id) {
		$this->db->where('id_user', $id);
		return $this->db->delete('m_user');
	}

	function insert_user_history($data){
		$this->db->insert('user_history', $data);
	}

	function insert_user_failed_login($data){
		$this->db->insert('user_failed_login', $data);
	}
}
