<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_pelanggan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_layanan_pelanggan(){
        $query = $this->db->get('layanan_pelanggan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_layanan_pelanggan($data){
        $this->db->insert('layanan_pelanggan', $data);
    }
    function select_layanan_pelanggan($id){
        $query = $this->db->get_where('layanan_pelanggan', array('id_layanan_pelanggan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_layanan_pelanggan($id, $data) {
        $this->db->where('id_layanan_pelanggan', $id);
        return $this->db->update('layanan_pelanggan', $data);
    }

    function select_all(){
        $query = $this->db->get('layanan_pelanggan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_pelanggan($id_profil_pelanggan){
        $this->db->select('*');
        $this->db->from('layanan_pelanggan');
        $this->db->join('m_jenis_layanan', 'layanan_pelanggan.id_jenis_layanan = m_jenis_layanan.id_jenis_layanan', 'left');
        $this->db->join('m_kategori_layanan', 'layanan_pelanggan.id_kategori_layanan = m_kategori_layanan.id_kategori_layanan', 'left');
        $this->db->join('m_layanan', 'layanan_pelanggan.id_layanan = m_layanan.id_layanan', 'left');
        $this->db->where('layanan_pelanggan.id_profil_pelanggan', $id_profil_pelanggan);
        $query = $this->db->get();
        $result_array = $query->result_array();

        // echo $this->db->last_query();
        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('layanan_pelanggan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM layanan_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('layanan_pelanggan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM layanan_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_layanan_pelanggan(){
        $query = $this->db->get('layanan_pelanggan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_layanan_pelanggan_search($where){
        $sql = " SELECT * FROM layanan_pelanggan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_layanan_pelanggan($id) {
        $this->db->where('id_layanan_pelanggan', $id);
        return $this->db->delete('layanan_pelanggan');
    }
}