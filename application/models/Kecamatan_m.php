<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_kecamatan(){
        $query = $this->db->get('m_kecamatan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_kecamatan($data){
        $this->db->insert('m_kecamatan', $data);
    }
    function select_detil_kecamatan($id){
        $query = $this->db->get_where('m_kecamatan', array('id_kecamatan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_kecamatan($id, $data) {
        $this->db->where('id_kecamatan', $id);
        return $this->db->update('m_kecamatan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_kecamatan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_kabupaten($id_kabupaten){
        $query = $this->db->get_where('m_kecamatan', array('id_kabupaten' => $id_kabupaten));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_kecamatan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_kecamatan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_kecamatan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_kecamatan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_kecamatan(){
        $query = $this->db->get('m_kecamatan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_kecamatan_search($where){
        $sql = " SELECT * FROM m_kecamatan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_kecamatan($id) {
        $this->db->where('id_kecamatan', $id);
        return $this->db->delete('m_kecamatan');
    }
}