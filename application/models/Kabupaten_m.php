<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_kabupaten(){
        $query = $this->db->get('m_kabupaten');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_kabupaten($data){
        $this->db->insert('m_kabupaten', $data);
    }
    function select_detil_kabupaten($id){
        $query = $this->db->get_where('m_kabupaten', array('id_kabupaten' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_kabupaten($id, $data) {
        $this->db->where('id_kabupaten', $id);
        return $this->db->update('m_kabupaten', $data);
    }

    function select_all(){
        $query = $this->db->get('m_kabupaten');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_provinsi($id_provinsi){
        $query = $this->db->get_where('m_kabupaten', array('id_provinsi' => $id_provinsi));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_kabupaten', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_kabupaten
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_kabupaten', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_kabupaten
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_kabupaten(){
        $query = $this->db->get('m_kabupaten');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_kabupaten_search($where){
        $sql = " SELECT * FROM m_kabupaten
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_kabupaten($id) {
        $this->db->where('id_kabupaten', $id);
        return $this->db->delete('m_kabupaten');
    }
}