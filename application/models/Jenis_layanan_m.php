<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_layanan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_jenis_layanan(){
        $query = $this->db->get('m_jenis_layanan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_jenis_layanan($data){
        $this->db->insert('m_jenis_layanan', $data);
    }
    function select_detil_jenis_layanan($id){
        $query = $this->db->get_where('m_jenis_layanan', array('id_jenis_layanan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_jenis_layanan($id, $data) {
        $this->db->where('id_jenis_layanan', $id);
        return $this->db->update('m_jenis_layanan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_jenis_layanan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_jenis_layanan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_jenis_layanan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_jenis_layanan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_jenis_layanan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_jenis_layanan(){
        $query = $this->db->get('m_jenis_layanan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_jenis_layanan_search($where){
        $sql = " SELECT * FROM m_jenis_layanan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_jenis_layanan($id) {
        $this->db->where('id_jenis_layanan', $id);
        return $this->db->delete('m_jenis_layanan');
    }
}