<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_kelurahan(){
        $query = $this->db->get('m_kelurahan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_kelurahan($data){
        $this->db->insert('m_kelurahan', $data);
    }
    function select_detil_kelurahan($id){
        $query = $this->db->get_where('m_kelurahan', array('id_kelurahan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_kelurahan($id, $data) {
        $this->db->where('id_kelurahan', $id);
        return $this->db->update('m_kelurahan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_kelurahan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_kecamatan($id_kecamatan){
        $query = $this->db->get_where('m_kelurahan', array('id_kecamatan' => $id_kecamatan));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_kelurahan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_kelurahan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_kelurahan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_kelurahan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_kelurahan(){
        $query = $this->db->get('m_kelurahan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_kelurahan_search($where){
        $sql = " SELECT * FROM m_kelurahan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_kelurahan($id) {
        $this->db->where('id_kelurahan', $id);
        return $this->db->delete('m_kelurahan');
    }
}