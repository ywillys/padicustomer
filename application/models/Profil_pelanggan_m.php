<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_pelanggan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_profil_pelanggan(){
        $query = $this->db->get('profil_pelanggan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_profil_pelanggan($data){
        $this->db->insert('profil_pelanggan', $data);
    }
    function select_detil_pelanggan($id){
        // $this->db->select('t1.field, t2.field2')->from('table1 AS t1, table2 AS t2');
        $this->db->select('*');
        // $this->db->select('t1.field, t2.field2');
        $this->db->from('profil_pelanggan');
        $this->db->join('m_sektor_industri', 'm_sektor_industri.id_sektor_industri = profil_pelanggan.id_sektor_industri', 'left');
        $this->db->join('m_provinsi', 'm_provinsi.id_provinsi = profil_pelanggan.id_provinsi', 'left');
        $this->db->join('m_kecamatan', 'm_kecamatan.id_kecamatan = profil_pelanggan.id_kecamatan', 'left');
        $this->db->join('m_kabupaten', 'm_kabupaten.id_kabupaten = profil_pelanggan.id_kabupaten', 'left');
        $this->db->join('m_kelurahan', 'm_kelurahan.id_kelurahan = profil_pelanggan.id_kelurahan', 'left');
        $this->db->join('m_jabatan_pic', 'm_jabatan_pic.id_jabatan_pic = profil_pelanggan.id_jabatan_dm', 'left');
        $this->db->join('m_fungsional_pic', 'm_fungsional_pic.id_fungsional_pic = profil_pelanggan.id_fungsional_dm', 'left');
        $this->db->join('m_jabatan_pic AS `m_jabatan_pic_2`', 'm_jabatan_pic_2.id_jabatan_pic = profil_pelanggan.id_jabatan_influ', 'left');
        $this->db->join('m_fungsional_pic AS `m_fungsional_pic_2`', 'm_fungsional_pic_2.id_fungsional_pic = profil_pelanggan.id_fungsional_influ', 'left');
        $this->db->join('m_kategori_berlangganan', 'm_kategori_berlangganan.id_kategori_berlangganan = profil_pelanggan.id_kategori_berlangganan', 'left');
        $this->db->where('profil_pelanggan.id_profil_pelanggan', $id);
        $query = $this->db->get();
        $result_array = $query->result_array();

        // echo $this->db->last_query();
        return $result_array;
    }

    function select_detil_profil_pelanggan($id){
        // $this->db->select('pp.*, mjab.nama_jabatan_pic as nama_jabatan_dm, mjab2.nama_jabatan_pic as influ, mfung.nama_fungsional_pic as nama_fungsional_dm, mfung2.nama_fungsional_pic as nama_fungsional_influ');
        $this->db->select('pp.*, msi.*, mprov.*, mkab.*, mkec.*, mkel.*, mjab.* , mjab2.*, mfung.*, mfung2.*, mkatber.*, mjab.nama_jabatan_pic as nama_jabatan_dm, mjab2.nama_jabatan_pic as nama_jabatan_influ, mfung.nama_fungsional_pic as nama_fungsional_dm, mfung2.nama_fungsional_pic as nama_fungsional_influ');
        $this->db->from('profil_pelanggan as pp');
        $this->db->join('m_sektor_industri as msi', 'msi.id_sektor_industri = pp.id_sektor_industri', 'left');
        $this->db->join('m_provinsi as mprov', 'mprov.id_provinsi = pp.id_provinsi', 'left');
        $this->db->join('m_kecamatan as mkec', 'mkec.id_kecamatan = pp.id_kecamatan', 'left');
        $this->db->join('m_kabupaten as mkab', 'mkab.id_kabupaten = pp.id_kabupaten', 'left');
        $this->db->join('m_kelurahan as mkel', 'mkel.id_kelurahan = pp.id_kelurahan', 'left');
        $this->db->join('m_jabatan_pic as mjab', 'mjab.id_jabatan_pic = pp.id_jabatan_dm', 'left');
        $this->db->join('m_fungsional_pic as  mfung', 'mfung.id_fungsional_pic = pp.id_fungsional_dm', 'left');
        $this->db->join('m_jabatan_pic as mjab2', 'mjab2.id_jabatan_pic = pp.id_jabatan_influ', 'left');
        $this->db->join('m_fungsional_pic as mfung2', 'mfung2.id_fungsional_pic = pp.id_fungsional_influ', 'left');
        $this->db->join('m_kategori_berlangganan as mkatber', 'mkatber.id_kategori_berlangganan = pp.id_kategori_berlangganan', 'left');
        $this->db->where('pp.id_profil_pelanggan', $id);
        $query = $this->db->get();
        $result_array = $query->result_array();

        // echo $this->db->last_query();
        return $result_array;
    }

    function filter_pelanggan($datafilter){
        $this->db->select('pp.*, msi.*, mprov.*, mkab.*, mkec.*, mkel.*, mkatber.*');
        $this->db->from('profil_pelanggan as pp');
        $this->db->join('m_sektor_industri as msi', 'msi.id_sektor_industri = pp.id_sektor_industri', 'left');
        $this->db->join('m_provinsi as mprov', 'mprov.id_provinsi = pp.id_provinsi', 'left');
        $this->db->join('m_kecamatan as mkec', 'mkec.id_kecamatan = pp.id_kecamatan', 'left');
        $this->db->join('m_kabupaten as mkab', 'mkab.id_kabupaten = pp.id_kabupaten', 'left');
        $this->db->join('m_kelurahan as mkel', 'mkel.id_kelurahan = pp.id_kelurahan', 'left');
        $this->db->join('m_kategori_berlangganan as mkatber', 'mkatber.id_kategori_berlangganan = pp.id_kategori_berlangganan', 'left');
        $this->db->where($datafilter);
        $query = $this->db->get();
        $result_array = $query->result_array();

        // $array = array('name !=' => $name, 'id <' => $id, 'date >' => $date);

        // $this->db->where($array);
        return $result_array;
    }

    function filter_layanan($datafilter){
        $this->db->distinct();
        $this->db->select('pp.*, msi.*, mprov.*, mkab.*, mkec.*, mkel.*, mkatber.*');
        $this->db->from('profil_pelanggan as pp');
        $this->db->join('m_sektor_industri as msi', 'msi.id_sektor_industri = pp.id_sektor_industri', 'left');
        $this->db->join('m_provinsi as mprov', 'mprov.id_provinsi = pp.id_provinsi', 'left');
        $this->db->join('m_kecamatan as mkec', 'mkec.id_kecamatan = pp.id_kecamatan', 'left');
        $this->db->join('m_kabupaten as mkab', 'mkab.id_kabupaten = pp.id_kabupaten', 'left');
        $this->db->join('m_kelurahan as mkel', 'mkel.id_kelurahan = pp.id_kelurahan', 'left');
        $this->db->join('m_kategori_berlangganan as mkatber', 'mkatber.id_kategori_berlangganan = pp.id_kategori_berlangganan', 'left');
        $this->db->join('layanan_pelanggan as lp', 'lp.id_profil_pelanggan = pp.id_profil_pelanggan', 'left');
        $this->db->join('m_jenis_layanan as mjlay', 'mjlay.id_jenis_layanan = lp.id_jenis_layanan');
        $this->db->join('m_kategori_layanan as mklay', 'mklay.id_kategori_layanan = lp.id_kategori_layanan');
        $this->db->join('m_layanan as mlay', 'mlay.id_layanan = lp.id_layanan');
        $this->db->where($datafilter);
        $query = $this->db->get();
        $result_array = $query->result_array();
        echo $this->db->last_query();
        return $result_array;
    }

    function filter_harga_layanan($datafilter, $datafilterharga){
        $this->db->select('pp.*, msi.*, mprov.*, mkab.*, mkec.*, mkel.*, mkatber.*, SUM(lp.nominal) AS total');
        $this->db->from('profil_pelanggan as pp');
        $this->db->join('m_sektor_industri as msi', 'msi.id_sektor_industri = pp.id_sektor_industri', 'left');
        $this->db->join('m_provinsi as mprov', 'mprov.id_provinsi = pp.id_provinsi', 'left');
        $this->db->join('m_kecamatan as mkec', 'mkec.id_kecamatan = pp.id_kecamatan', 'left');
        $this->db->join('m_kabupaten as mkab', 'mkab.id_kabupaten = pp.id_kabupaten', 'left');
        $this->db->join('m_kelurahan as mkel', 'mkel.id_kelurahan = pp.id_kelurahan', 'left');
        $this->db->join('m_kategori_berlangganan as mkatber', 'mkatber.id_kategori_berlangganan = pp.id_kategori_berlangganan', 'left');
        $this->db->join('layanan_pelanggan as lp', 'lp.id_profil_pelanggan = pp.id_profil_pelanggan', 'left');
        $this->db->join('m_jenis_layanan as mjlay', 'mjlay.id_jenis_layanan = lp.id_jenis_layanan', 'left');
        $this->db->join('m_kategori_layanan as mklay', 'mklay.id_kategori_layanan = lp.id_kategori_layanan', 'left');
        $this->db->join('m_layanan as mlay', 'mlay.id_layanan = lp.id_layanan', 'left');
        
        if(!empty($datafilter)){
            $this->db->where($datafilter);
        }

        $this->db->group_by('pp.id_profil_pelanggan');

        if(!empty($datafilterharga)){
            $this->db->having($datafilterharga);
        }
        
        $query = $this->db->get();
        $result_array = $query->result_array();

        // echo $this->db->last_query();

        return $result_array;
    }


    function update_profil_pelanggan($id, $data) {
        $this->db->where('id_profil_pelanggan', $id);
        return $this->db->update('profil_pelanggan', $data);
    }

    function select_all(){
        $this->db->select('*');
        $this->db->from('profil_pelanggan');
        $this->db->join('m_sektor_industri', 'm_sektor_industri.id_sektor_industri = profil_pelanggan.id_sektor_industri', 'left');
        $this->db->join('m_provinsi', 'm_provinsi.id_provinsi = profil_pelanggan.id_provinsi', 'left');
        $this->db->join('m_kecamatan', 'm_kecamatan.id_kecamatan = profil_pelanggan.id_kecamatan', 'left');
        $this->db->join('m_kabupaten', 'm_kabupaten.id_kabupaten = profil_pelanggan.id_kabupaten', 'left');
        $this->db->join('m_kelurahan', 'm_kelurahan.id_kelurahan = profil_pelanggan.id_kelurahan', 'left');
        $this->db->join('m_jabatan_pic', 'm_jabatan_pic.id_jabatan_pic = profil_pelanggan.id_jabatan_dm', 'left');
        $this->db->join('m_fungsional_pic', 'm_fungsional_pic.id_fungsional_pic = profil_pelanggan.id_fungsional_dm', 'left');
        $this->db->join('m_jabatan_pic AS `m_jabatan_pic_2`', 'm_jabatan_pic_2.id_jabatan_pic = profil_pelanggan.id_jabatan_influ', 'left');
        $this->db->join('m_fungsional_pic AS `m_fungsional_pic_2`', 'm_fungsional_pic_2.id_fungsional_pic = profil_pelanggan.id_fungsional_influ', 'left');
        $this->db->join('m_kategori_berlangganan', 'm_kategori_berlangganan.id_kategori_berlangganan = profil_pelanggan.id_kategori_berlangganan', 'left');
        $query = $this->db->get();
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('profil_pelanggan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM profil_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('profil_pelanggan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM profil_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_profil_pelanggan(){
        $query = $this->db->get('profil_pelanggan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_profil_pelanggan_search($where){
        $sql = " SELECT * FROM profil_pelanggan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_profil_pelanggan($id) {
        $this->db->where('id_profil_pelanggan', $id);
        return $this->db->delete('profil_pelanggan');
    }
}