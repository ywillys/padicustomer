<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_layanan(){
        $query = $this->db->get('m_layanan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_layanan($data){
        $this->db->insert('m_layanan', $data);
    }
    function select_detil_layanan($id){
        $query = $this->db->get_where('m_layanan', array('id_layanan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_layanan($id, $data) {
        $this->db->where('id_layanan', $id);
        return $this->db->update('m_layanan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_layanan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_kategori($id_kategori_layanan){
        $query = $this->db->get_where('m_layanan', array('id_kategori_layanan' => $id_kategori_layanan));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_layanan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_layanan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_layanan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_layanan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_layanan(){
        $query = $this->db->get('m_layanan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_layanan_search($where){
        $sql = " SELECT * FROM m_layanan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_layanan($id) {
        $this->db->where('id_layanan', $id);
        return $this->db->delete('m_layanan');
    }
}