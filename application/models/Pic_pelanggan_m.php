<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pic_pelanggan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_pic_pelanggan(){
        $query = $this->db->get('pic_pelanggan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_pic_pelanggan($data){
        $this->db->insert('pic_pelanggan', $data);
    }
    function select_detil_pic_pelanggan($id){
        $query = $this->db->get_where('pic_pelanggan', array('id_pic_pelanggan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_pic_pelanggan($id, $data) {
        $this->db->where('id_pic_pelanggan', $id);
        return $this->db->update('pic_pelanggan', $data);
    }

    function select_all_pelanggan($id_profil_pelanggan){
        $query = $this->db->get_where('pic_pelanggan', array('id_profil_pelanggan' => $id_profil_pelanggan));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all(){
        $query = $this->db->get('pic_pelanggan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_provinsi($id_provinsi){
        $query = $this->db->get_where('pic_pelanggan', array('id_provinsi' => $id_provinsi));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('pic_pelanggan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM pic_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('pic_pelanggan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM pic_pelanggan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_pic_pelanggan(){
        $query = $this->db->get('pic_pelanggan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_pic_pelanggan_search($where){
        $sql = " SELECT * FROM pic_pelanggan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_pic_pelanggan($id) {
        $this->db->where('id_pic_pelanggan', $id);
        return $this->db->delete('pic_pelanggan');
    }
}