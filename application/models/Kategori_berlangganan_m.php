<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_berlangganan_m  extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    function select_jum_kategori_berlangganan(){
        $query = $this->db->get('m_kategori_berlangganan');
        $hasil['result'] = $query->result_array();
        $hasil['jumlah'] = $query->num_rows();
        return $hasil;
    }
    function insert_kategori_berlangganan($data){
        $this->db->insert('m_kategori_berlangganan', $data);
    }
    function select_detil_kategori_berlangganan($id){
        $query = $this->db->get_where('m_kategori_berlangganan', array('id_kategori_berlangganan' => $id));
        $result_array = $query->result_array();

        return $result_array;
    }
    function update_kategori_berlangganan($id, $data) {
        $this->db->where('id_kategori_berlangganan', $id);
        return $this->db->update('m_kategori_berlangganan', $data);
    }

    function select_all(){
        $query = $this->db->get('m_kategori_berlangganan');//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_active(){
        $query = $this->db->get_where('m_kategori_berlangganan', array('status_aktif' => 1));
        $result_array = $query->result_array();

        return $result_array;
    }

    function select_all_search($where, $orderby, $ordertype){
        $sql = " SELECT * FROM m_kategori_berlangganan
             ".$where."
              ".$orderby." ".$ordertype."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function select_allpaging($limit,$offset){
        $query = $this->db->get('m_kategori_berlangganan', $limit, $offset);//namatabel
        $result_array = $query->result_array();

        return $result_array;
    }
    function select_allpaging_search($where, $orderby, $ordertype, $limit, $offset){
        $sql = " SELECT * FROM m_kategori_berlangganan
             ".$where."
              ".$orderby." ".$ordertype."
              LIMIT ".$limit." OFFSET ".$offset."
        ";
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;

    }
    function jum_kategori_berlangganan(){
        $query = $this->db->get('m_kategori_berlangganan');
        $jum = $query->num_rows();
        return $jum;
    }
    function jum_kategori_berlangganan_search($where){
        $sql = " SELECT * FROM m_kategori_berlangganan
              ".$where."
        ";
        $query = $this->db->query($sql);
        $jum = $query->num_rows();
        return $jum;
    }

    function delete_kategori_berlangganan($id) {
        $this->db->where('id_kategori_berlangganan', $id);
        return $this->db->delete('m_kategori_berlangganan');
    }
}