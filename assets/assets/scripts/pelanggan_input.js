$( document ).ready(function() {
    $('.pelanggan-update').hide();
    $('.selectpelanggan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectprovinsi').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectsektorindustri').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectlevelperusahaan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectkategoriberlangganan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectfungsionaldm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabatandm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    
    $('.selectfungsionalinflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabataninflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $(".selectpelanggan").change(function(){
        
        var id_pelanggan = $( ".selectpelanggan option:selected" ).val();
        $( ".pelanggan-update" ).slideDown( "slow");
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_alamat_pelanggan/"+id_pelanggan,
            type: 'GET',
            success: function(result) {
                $( ".text-area-alamat_pelanggan" ).val(result);
            }
        });
        $(".nama-pelanggan-update" ).val('');
        $(".alamat-pelanggan-update" ).val('');
       
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_pic_pelanggan/"+id_pelanggan,
            type: 'GET',
            success: function(result) {
                var alldata = JSON.parse(result);
                for(var i=0 ; i<5; i++){
                    if(alldata[i]['role'] == 'resp'){
                        $("#nama_pemohon" ).val(alldata[i]['pic']);
                        $("#no_telp_pemohon" ).val(alldata[i]['phone']);
                        $("#email_pemohon" ).val(alldata[i]['email']);
                        $("#role_pemohon" ).val(alldata[i]['role']);
                    }
                    if(alldata[i]['role'] == 'subscriber'){
                        $("#nama_penanggung" ).val(alldata[i]['pic']);
                        $("#no_telp_penanggung" ).val(alldata[i]['phone']);
                        $("#email_penanggung" ).val(alldata[i]['email']);
                        $("#role_penanggung" ).val(alldata[i]['role']);
                    }
                    if(alldata[i]['role'] == 'adm'){
                        $("#nama_adm" ).val(alldata[i]['pic']);
                        $("#no_telp_adm" ).val(alldata[i]['phone']);
                        $("#email_adm" ).val(alldata[i]['email']);
                        $("#role_adm" ).val(alldata[i]['role']);
                    }
                    if(alldata[i]['role'] == 'support'){
                        $("#nama_support" ).val(alldata[i]['pic']);
                        $("#no_telp_support" ).val(alldata[i]['phone']);
                        $("#email_support" ).val(alldata[i]['email']);
                        $("#role_support" ).val(alldata[i]['role']);
                    }
                    if(alldata[i]['role'] == 'teknis'){
                        $("#nama_teknis" ).val(alldata[i]['pic']);
                        $("#no_telp_teknis" ).val(alldata[i]['phone']);
                        $("#email_teknis" ).val(alldata[i]['email']);
                        $("#role_teknis" ).val(alldata[i]['role']);
                    }
                    if(alldata[i]['role'] == 'billing'){
                        $("#nama_billing" ).val(alldata[i]['pic']);
                        $("#no_telp_billing" ).val(alldata[i]['phone']);
                        $("#email_billing" ).val(alldata[i]['email']);
                        $("#role_billing" ).val(alldata[i]['role']);
                    }
                }
                // console.log(alldata[0]['role']);
                // $.each(alldata , function( index, obj ) {
                //     $.each(obj, function( key, value ) {
                //         if(key)
                //         console.log(key);
                //         console.log(value);
                //     });
                // });
            }
        });
    });
    
    $(".selectprovinsi").change(function(){
        // alert(a);
        var id_provinsi = $( ".selectprovinsi option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kabupaten/"+id_provinsi,
            type: 'GET',
            success: function(result) {
                $( ".kabupaten-show" ).html(result);

                $('.selectkabupaten').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkabupaten").change(function(){
        var id_kabupaten = $( ".selectkabupaten option:selected" ).val();
        
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kecamatan/"+id_kabupaten,
            type: 'GET',
            success: function(result) {
                $( ".kecamatan-show" ).html(result);

                $('.selectkecamatan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkecamatan").change(function(){
        var id_kecamatan = $( ".selectkecamatan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kelurahan/"+id_kecamatan,
            type: 'GET',
            success: function(result) {
                $( ".kelurahan-show" ).html(result);

                $('.selectkelurahan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });
    
    $('.btn-nama-pelanggan').click(function(){
        var nama_pelanggan = $( ".selectpelanggan option:selected" ).text();
        $( ".nama-pelanggan-update" ).val(nama_pelanggan);
    }); 
    $('.btn-alamat-pelanggan').click(function(){
        var alamat_pelanggan = $( ".text-area-alamat_pelanggan" ).val();
        $( ".alamat-pelanggan-update" ).val(alamat_pelanggan);
    }); 

    

    $('.selectkategorilayanan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $(".selectkategorilayanan").change(function(){
        var id_kat_layanan = $( ".selectkategorilayanan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_layanan/"+id_kat_layanan,
            type: 'GET',
            success: function(result) {
                $( ".show-layanan" ).html(result);

                $('.selectlayanan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    
});