//edit form
$( document ).ready(function() {
    var mode_data = 'edit';
    $('.selectpelanggan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectprovinsi').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectsektorindustri').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectlevelperusahaan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectkategoriberlangganan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectfungsionaldm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabatandm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    
    $('.selectfungsionalinflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabataninflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    
    var id_pelanggan = $(".selectpelanggan option:selected" ).val();
    $.ajax({
        url: BASEURLPHP+"admin/customer/get_alamat_pelanggan/"+id_pelanggan,
        type: 'GET',
        success: function(result) {
            $( ".text-area-alamat_pelanggan" ).val(result);
        }
    });

    var id_provinsi = $( ".selectprovinsi option:selected" ).val();
    $.ajax({
        url: BASEURLPHP+"admin/customer/get_kabupaten/"+id_provinsi+"/"+mode_data+"/"+ID_PROFIL_PELANGGAN,
        type: 'GET',
        success: function(result) {
            $( ".kabupaten-show" ).html(result);

            $('.selectkabupaten').select2({ 
                width: '100%',
                allowClear: true,
                theme: "bootstrap"
            });

            var id_kabupaten = $( ".selectkabupaten option:selected" ).val();
            $.ajax({
                url: BASEURLPHP+"admin/customer/get_kecamatan/"+id_kabupaten+"/"+mode_data+"/"+ID_PROFIL_PELANGGAN,
                type: 'GET',
                success: function(result) {
                    $( ".kecamatan-show" ).html(result);
        
                    $('.selectkecamatan').select2({ 
                        width: '100%',
                        allowClear: true,
                        theme: "bootstrap"
                    });

                    var id_kecamatan = $( ".selectkecamatan option:selected" ).val();
                    $.ajax({
                        url: BASEURLPHP+"admin/customer/get_kelurahan/"+id_kecamatan+"/"+mode_data+"/"+ID_PROFIL_PELANGGAN,
                        type: 'GET',
                        success: function(result) {
                            $( ".kelurahan-show" ).html(result);

                            $('.selectkelurahan').select2({ 
                                width: '100%',
                                allowClear: true,
                                theme: "bootstrap"
                            });
                        }
                    });
                }
            });
        }
    }); 
    $(".selectpelanggan").change(function(){
        
        var id_pelanggan = $( ".selectpelanggan option:selected" ).val();
        $( ".pelanggan-update" ).slideDown( "slow");
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_alamat_pelanggan/"+id_pelanggan,
            type: 'GET',
            success: function(result) {
                $( ".text-area-alamat_pelanggan" ).val(result);
            }
        });
        $(".nama-pelanggan-update" ).val('');
        $(".alamat-pelanggan-update" ).val('');
    });
    
    $(".selectprovinsi").change(function(){
        // alert(a);
        var id_provinsi = $( ".selectprovinsi option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kabupaten/"+id_provinsi,
            type: 'GET',
            success: function(result) {
                $( ".kabupaten-show" ).html(result);

                $('.selectkabupaten').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkabupaten").change(function(){
        var id_kabupaten = $( ".selectkabupaten option:selected" ).val();
        
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kecamatan/"+id_kabupaten,
            type: 'GET',
            success: function(result) {
                $( ".kecamatan-show" ).html(result);

                $('.selectkecamatan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkecamatan").change(function(){
        var id_kecamatan = $( ".selectkecamatan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kelurahan/"+id_kecamatan,
            type: 'GET',
            success: function(result) {
                $( ".kelurahan-show" ).html(result);

                $('.selectkelurahan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });
    
    $('.btn-nama-pelanggan').click(function(){
        var nama_pelanggan = $( ".selectpelanggan option:selected" ).text();
        $( ".nama-pelanggan-update" ).val(nama_pelanggan);
    }); 
    $('.btn-alamat-pelanggan').click(function(){
        var alamat_pelanggan = $( ".text-area-alamat_pelanggan" ).val();
        $( ".alamat-pelanggan-update" ).val(alamat_pelanggan);
    }); 

    $('.form-layanan').hide();
    
    $('.btn-add-layanan').click(function(){
        $( ".form-layanan" ).slideDown( "slow");
        $( ".btn-add-layanan" ).slideUp("slow");
    });

    $('.btn-cancel-layanan').click(function(){
        $( ".form-layanan" ).slideUp("slow");
        $( ".btn-add-layanan" ).slideDown( "slow");
    });


    
});    
