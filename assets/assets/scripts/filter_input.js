$( document ).ready(function() {
    $('.selectprovinsi').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectsektorindustri').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectlevelperusahaan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectkategoriberlangganan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    $('.selectfungsionaldm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabatandm').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    
    $('.selectfungsionalinflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectjabataninflu').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $(".selectprovinsi").change(function(){
        // alert(a);
        var id_provinsi = $( ".selectprovinsi option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kabupaten/"+id_provinsi,
            type: 'GET',
            success: function(result) {
                $( ".kabupaten-show" ).html(result);

                $('.selectkabupaten').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkabupaten").change(function(){
        var id_kabupaten = $( ".selectkabupaten option:selected" ).val();
        
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kecamatan/"+id_kabupaten,
            type: 'GET',
            success: function(result) {
                $( ".kecamatan-show" ).html(result);

                $('.selectkecamatan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkecamatan").change(function(){
        var id_kecamatan = $( ".selectkecamatan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kelurahan/"+id_kecamatan,
            type: 'GET',
            success: function(result) {
                $( ".kelurahan-show" ).html(result);

                $('.selectkelurahan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });
    
    $('.btn-nama-pelanggan').click(function(){
        var nama_pelanggan = $( ".selectpelanggan option:selected" ).text();
        $( ".nama-pelanggan-update" ).val(nama_pelanggan);
    }); 
    $('.btn-alamat-pelanggan').click(function(){
        var alamat_pelanggan = $( ".text-area-alamat_pelanggan" ).val();
        $( ".alamat-pelanggan-update" ).val(alamat_pelanggan);
    }); 

    

    $('.selectjenislayanan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });
    
    $(".selectjenislayanan").change(function(){
        var id_jenis_layanan = $( ".selectjenislayanan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kategori_layanan/"+id_jenis_layanan,
            type: 'GET',
            success: function(result) {
                $( ".show-kategori-layanan" ).html(result);

                $('.selectkategorilayanan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkategorilayanan").change(function(){
        var id_kat_layanan = $( ".selectkategorilayanan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_layanan/"+id_kat_layanan,
            type: 'GET',
            success: function(result) {
                $( ".show-layanan" ).html(result);

                $('.selectlayanan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    
});