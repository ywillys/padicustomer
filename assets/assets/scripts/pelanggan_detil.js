$( document ).ready(function() {

    $('.form-layanan').hide();
    
    $('.btn-add-layanan').click(function(){
        $( ".form-layanan" ).slideDown( "slow");
        $( ".btn-add-layanan" ).slideUp("slow");
    });

    $('.btn-cancel-layanan').click(function(){
        $( ".form-layanan" ).slideUp("slow");
        $( ".btn-add-layanan" ).slideDown( "slow");
    });


    $('.selectjenislayanan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $('.selectkategorilayanan').select2({ 
        width: '100%',
        allowClear: true,
        theme: "bootstrap"
    });

    $(".selectjenislayanan").change(function(){
        var id_jenis_layanan = $( ".selectjenislayanan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_kategori_layanan/"+id_jenis_layanan,
            type: 'GET',
            success: function(result) {
                $( ".show-kategori-layanan" ).html(result);

                $('.selectkategorilayanan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    $(".selectkategorilayanan").change(function(){
        var id_kat_layanan = $( ".selectkategorilayanan option:selected" ).val();
        $.ajax({
            url: BASEURLPHP+"admin/customer/get_layanan/"+id_kat_layanan,
            type: 'GET',
            success: function(result) {
                $( ".show-layanan" ).html(result);

                $('.selectlayanan').select2({ 
                    width: '100%',
                    allowClear: true,
                    theme: "bootstrap"
                });
            }
        });
    });

    // $('.edit-layanan-pelanggan').click(function(){
    //     alert($(this).closest('tr').attr("id"));
    // });
});